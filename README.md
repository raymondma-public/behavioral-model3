# Setup environment
1) Go to the project folder

```
#!shell

cd /behavioral-model3/

```
2) Install the dependency

```
#!shell

sudo ./install_deps.sh
```
3) Install the remaining component

```
#!shell

cd travis
sudo ./install-thrift.sh
sudo ./install-nnpy.sh
sudo ./install-nanomsg.sh

```
4) You may need to build for the first time

```
#!shell

./autogen.sh
./configure --disable-logging-macros --disable-elogger
make

```

#Enhance switch performance on Cloudlab.us test bed
Turning off the following option in switch interfaces can enhance performance.

```
#!shell

ethtool -K ethX tso off gso off gro off
```

#Prevent forwarding with Ubuntu routing

```
#!shell

sudo route del -net <ip>/<netmask>
```

#Hosts setting for testing
Add static ARP( prevent ARP request) of switch to prevent ARP request as P4 switch does not handle ARP request. eg.

```
#!shell

sudo arp -s 10.0.4.1 00:0c:00:00:04:00
```