/* Copyright 2013-present Barefoot Networks, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Antonin Bas (antonin@barefootnetworks.com)
 *
 */

#include <bm/bm_sim/actions.h>
#include <bm/bm_sim/switch.h>
#include <simple_switch.h>
#include <random>

//for logger
#include <sstream>

#include <bm/bm_sim/logger.h>

//vector
#include <vector>


//CRC
#include <bm/bm_sim/calculations.h>
#include <bm/bm_sim/parser.h>

#include <fstream>
#include <string>

template<typename ... Args>
using ActionPrimitive = bm::ActionPrimitive<Args...>;

using bm::Data;
using bm::Field;
using bm::Header;
using bm::MeterArray;
using bm::CounterArray;
using bm::RegisterArray;
using bm::NamedCalculation;
using bm::HeaderStack;

//
using bm::CalculationsMap;

//using namespace bm; //this should not be use

class modify_field: public ActionPrimitive<Data &, const Data &> {
	void operator ()(Data &dst, const Data &src) {
		dst.set(src);
	}
};

REGISTER_PRIMITIVE(modify_field);

class my_modify_field: public ActionPrimitive<Data &, const Data &> {
	void operator ()(Data &dst, const Data &src) {
//	  Logger::get()->trace(src);

//	  src.set(dst);

		SimpleSwitch* sw = SimpleSwitch::getInstance();
//		long congestionData = sw->getPortCongestionData(1); // get congestion data from port 1

		unsigned long long finalTrasnmitCD = 0;
				for (int i = 1; i <= sw->MAX_UPSTREAM; i++) { //for every port
					finalTrasnmitCD <<= 4;
					finalTrasnmitCD = finalTrasnmitCD+ sw->getCongestionDataForTransmit(i);
				}


		//	  const  Data &  d1(congestionData);
////	  src.set(congestionData);
//	  src.set(congestionData);
		dst.set(src);
		dst.set(finalTrasnmitCD);

	}
};

REGISTER_PRIMITIVE(my_modify_field);

#define UNUSED(x) (void)(x)
//
class compute_expeditus_egress_spec: public ActionPrimitive<Data &, const Data &,const Data &,const Data &,const Data &,const Data &,const Data &,const Data &> {
	void operator ()(Data &egress, const Data &congestionData, const Data &num_up_stream, const Data &srcIp, const Data &srcPort,const Data &dstIp,const Data &dstPort ,const Data &protocol) {
//	  Logger::get()->trace(src);
		std::ofstream outfile;
		outfile.open("CD.txt", std::ios_base::app);

//	  src.set(dst);
		SimpleSwitch* sw = SimpleSwitch::getInstance();
		int numUpstream=num_up_stream.get_int();
		std::atomic<size_t> portCongestionData[sw->MAX_UPSTREAM+1];//value 0-15
		std::atomic<size_t> portCongestionDataSummary[sw->MAX_UPSTREAM+1];//value 0-15
		unsigned long long numberForMinus=0;
		unsigned long long wholeCongestionData=congestionData.get_uint64();

//		int MAX_UPSTREAM=16;
		//converting whole congestion data into separate element
		for(int i=1;i<=sw->MAX_UPSTREAM;i++){
//			portCongestionData[i]=(wholeCongestionData<<((i-1)*4))>>(numUpstream-1);
			unsigned long long currentNotShifted= wholeCongestionData-numberForMinus;
			int bitNeedToShift=(((sw->MAX_UPSTREAM-i))*4);//1 Hex is 4 bit, 4 bit is one port
			unsigned long long currentShiftedRight= currentNotShifted>>bitNeedToShift;
				portCongestionData[i]=currentShiftedRight;


			portCongestionDataSummary[i]=portCongestionData[i]+ sw->getCongestionDataForTransmit(i);
			outfile << "port "<<std::to_string(i)<<" "<<std::to_string(portCongestionDataSummary[i])<<" = "<<std::to_string(portCongestionData[i])<<" + "<< std::to_string(sw->getCongestionDataForTransmit(i)) <<"\n";
			unsigned long long currentShiftedBackForAccumulate=currentShiftedRight<<bitNeedToShift;// remove the right side
			numberForMinus+=currentShiftedBackForAccumulate;
		}

		unsigned int min=30;//src sw + dst sw
		int egress_port=1;//this should be hash as ecmp
		for(int i=1;i<=numUpstream;i++){
				if(portCongestionDataSummary[i]<min){
					min=portCongestionDataSummary[i];
					egress_port=i;
				}
		}

//
//		std::stringstream stream;
//		stream<<std::hex<<egress_port;
//		std::string egressPortString(stream.str());


		int sourceIP=srcIp.get_int();
		int sourcePort=srcPort.get_int();
		int destinationIP=dstIp.get_int();
		int destinationPort=dstPort.get_int();
		int ipProtocol=protocol.get_int();

		UNUSED(egress_port);
		UNUSED(sourceIP);
		UNUSED(sourcePort);
		UNUSED(destinationIP);
		UNUSED(destinationPort);
		UNUSED(ipProtocol);

		std::vector<int> sameMinValuePorts;

		for(int i=1;i<=numUpstream;i++){
			if(portCongestionDataSummary[i]==min){
				sameMinValuePorts.push_back(i);
			}
		}

//		NamedCalculation crc16("crc16");

		  const auto ptr = CalculationsMap::get_instance()->get_copy("crc16");

		  const unsigned char input_buffer[] = {(unsigned char)sourceIP, (unsigned char)sourcePort,(unsigned char) destinationIP, (unsigned char)destinationPort,(unsigned char)ipProtocol};

		  const uint16_t crc16Output = ptr->output(
		      reinterpret_cast<const char *>(input_buffer), sizeof(input_buffer));

		  int valueForGettingPort=crc16Output % sameMinValuePorts.size();
		  egress_port=sameMinValuePorts.at(valueForGettingPort);







			Data egressPortData(egress_port);
			std::string egressPortString=egressPortData.get_string();

			std::string sourceIpString=srcIp.get_string();
			std::string sourcePortString=srcPort.get_string();
			std::string destinationIp=dstIp.get_string();
			std::string destinationPortString=dstPort.get_string();
			std::string ipProtocolString=protocol.get_string();
//		sw->addEgressToForwardingTable(egress_port,sourceIP,sourcePort,destinationIP,destinationPort,ipProtocol);
		sw->addEgressToForwardingTable(egressPortString,sourceIpString,sourcePortString,destinationIp,destinationPortString,ipProtocolString);
//		egress
		outfile <<"egress: "<<std::to_string(egress_port) <<"\n" ;
		egress.set(egress_port);


	}
};

REGISTER_PRIMITIVE(compute_expeditus_egress_spec);


class torAddEntry: public ActionPrimitive<const Data &,const Data &,const Data &,const Data &,const Data &,const Data &> {
	void operator ()(const Data &egress, const Data &srcIp, const Data &srcPort,const Data &dstIp,const Data &dstPort ,const Data &protocol) {
//	  Logger::get()->trace(src);

//	  src.set(dst);
		SimpleSwitch* sw = SimpleSwitch::getInstance();
//		int numUpstream=num_up_stream.get_int();
//		std::atomic<size_t> portCongestionData[numUpstream+1];//value 0-15
//		std::atomic<size_t> portCongestionDataSummary[numUpstream+1];//value 0-15
//		unsigned long long numberForMinus=0;
//		unsigned long long wholeCongestionData=congestionData.get_uint64();
//		for(int i=0;i<numUpstream;i++){
////			portCongestionData[i]=(wholeCongestionData<<((i-1)*4))>>(numUpstream-1);
//			unsigned long long currentNotShifted= wholeCongestionData-numberForMinus;
//			int bitNeedToShift=(((numUpstream-i-1))*8);//2 Hex is 8 bit
//			unsigned long long currentShiftedRight= currentNotShifted>>bitNeedToShift;
//				portCongestionData[i]=currentShiftedRight;
//
//
//			portCongestionDataSummary[i]=portCongestionData[i]+ sw->getCongestionDataForTransmit(i);
//			unsigned long long currentShiftedBackForAccumulate=currentShiftedRight<<bitNeedToShift;// remove the right side
//			numberForMinus+=currentShiftedBackForAccumulate;
//		}
//
//		unsigned int min=30;//src sw + dst sw
//		int egress_port=1;//this should be hash as ecmp
//		for(int i=1;i<=numUpstream;i++){
//				if(portCongestionDataSummary[i]<min){
//					min=portCongestionDataSummary[i];
//					egress_port=i;
//				}
//		}

//
//		std::stringstream stream;
//		stream<<std::hex<<egress_port;
//		std::string egressPortString(stream.str());
//		Data egressPortData(egress_port);
		std::string egressPortString=egress.get_string();

		std::string sourceIpString=srcIp.get_string();
		std::string sourcePortString=srcPort.get_string();
		std::string destinationIp=dstIp.get_string();
		std::string destinationPortString=dstPort.get_string();
		std::string ipProtocolString=protocol.get_string();

//		long sourceIP=srcIp.get_int();
//		long sourcePort=srcPort.get_int();
//		long destinationIP=dstIp.get_int();
//		long destinationPort=dstPort.get_int();
//		long ipProtocol=protocol.get_int();

//		UNUSED(egress_port);
//		UNUSED(sourceIP);
//		UNUSED(sourcePort);
//		UNUSED(destinationIP);
//		UNUSED(destinationPort);
//		UNUSED(ipProtocol);

//		sw->addEgressToForwardingTable(egress_port,sourceIP,sourcePort,destinationIP,destinationPort,ipProtocol);
		sw->addEgressToForwardingTable(egressPortString,sourceIpString,sourcePortString,destinationIp,destinationPortString,ipProtocolString);
//		egress
//		egress.set(egress_port);


	}
};

REGISTER_PRIMITIVE(torAddEntry);


class set_thrift_port: public ActionPrimitive<const Data &> {
	void operator ()(const Data &thriftPort) {

		SimpleSwitch* sw = SimpleSwitch::getInstance();
		sw-> setThriftPort(thriftPort.get_int());

	}
};

REGISTER_PRIMITIVE(set_thrift_port);



class modify_field_rng_uniform: public ActionPrimitive<Data &, const Data &,
		const Data &> {
	void operator ()(Data &f, const Data &b, const Data &e) {
		// TODO(antonin): a little hacky, fix later if there is a need using GMP
		// random fns
		using engine = std::default_random_engine;
		using hash = std::hash<std::thread::id>;
		static thread_local engine generator(
				hash()(std::this_thread::get_id()));
		using distrib64 = std::uniform_int_distribution<uint64_t>;
		distrib64 distribution(b.get_uint64(), e.get_uint64());
		f.set(distribution(generator));
	}
};

REGISTER_PRIMITIVE(modify_field_rng_uniform);

class add_to_field: public ActionPrimitive<Field &, const Data &> {
	void operator ()(Field &f, const Data &d) {
		f.add(f, d);
	}
};

REGISTER_PRIMITIVE(add_to_field);

class subtract_from_field: public ActionPrimitive<Field &, const Data &> {
	void operator ()(Field &f, const Data &d) {
		f.sub(f, d);
	}
};

REGISTER_PRIMITIVE(subtract_from_field);

class add: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.add(d1, d2);
	}
};

REGISTER_PRIMITIVE(add);

class subtract: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.sub(d1, d2);
	}
};

REGISTER_PRIMITIVE(subtract);

class bit_xor: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.bit_xor(d1, d2);
	}
};

REGISTER_PRIMITIVE(bit_xor);

class bit_or: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.bit_or(d1, d2);
	}
};

REGISTER_PRIMITIVE(bit_or);

class bit_and: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.bit_and(d1, d2);
	}
};

REGISTER_PRIMITIVE(bit_and);

class shift_left: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.shift_left(d1, d2);
	}
};

REGISTER_PRIMITIVE(shift_left);

class shift_right: public ActionPrimitive<Data &, const Data &, const Data &> {
	void operator ()(Data &f, const Data &d1, const Data &d2) {
		f.shift_right(d1, d2);
	}
};

REGISTER_PRIMITIVE(shift_right);

class drop: public ActionPrimitive<> {
	void operator ()() {
		get_field("standard_metadata.egress_spec").set(511);
		if (get_phv().has_header("intrinsic_metadata")) {
			get_field("intrinsic_metadata.mcast_grp").set(0);
		}
	}
};

REGISTER_PRIMITIVE(drop);

class exit_: public ActionPrimitive<> {
	void operator ()() {
		get_packet().mark_for_exit();
	}
};

REGISTER_PRIMITIVE_W_NAME("exit", exit_);

class generate_digest: public ActionPrimitive<const Data &, const Data &> {
	void operator ()(const Data &receiver, const Data &learn_id) {
		// discared receiver for now
		(void) receiver;
		get_field("intrinsic_metadata.lf_field_list").set(learn_id);
	}
};

REGISTER_PRIMITIVE(generate_digest);

class add_header: public ActionPrimitive<Header &> {
	void operator ()(Header &hdr) {
		// TODO(antonin): reset header to 0?
		if (!hdr.is_valid()) {
			hdr.reset();
			hdr.mark_valid();
			// updated the length packet register (register 0)
			auto &packet = get_packet();
			packet.set_register(0,
					packet.get_register(0) + hdr.get_nbytes_packet());
		}
	}
};

REGISTER_PRIMITIVE(add_header);

class add_header_fast: public ActionPrimitive<Header &> {
	void operator ()(Header &hdr) {
		hdr.mark_valid();
	}
};

REGISTER_PRIMITIVE(add_header_fast);

class remove_header: public ActionPrimitive<Header &> {
	void operator ()(Header &hdr) {
		if (hdr.is_valid()) {
			// updated the length packet register (register 0)
			auto &packet = get_packet();
			packet.set_register(0,
					packet.get_register(0) - hdr.get_nbytes_packet());
			hdr.mark_invalid();
		}
	}
};

REGISTER_PRIMITIVE(remove_header);

class copy_header: public ActionPrimitive<Header &, const Header &> {
	void operator ()(Header &dst, const Header &src) {
		if (!src.is_valid()) {
			dst.mark_invalid();
			return;
		}
		dst.mark_valid();
		assert(dst.get_header_type_id() == src.get_header_type_id());
		for (unsigned int i = 0; i < dst.size(); i++) {
			dst[i].set(src[i]);
		}
	}
};

REGISTER_PRIMITIVE(copy_header);

/* standard_metadata.clone_spec will contain the mirror id (16 LSB) and the
 field list id to copy (16 MSB) */
class clone_ingress_pkt_to_egress: public ActionPrimitive<const Data &,
		const Data &> {
	void operator ()(const Data &clone_spec, const Data &field_list_id) {
		Field &f_clone_spec = get_field("standard_metadata.clone_spec");
		f_clone_spec.shift_left(field_list_id, 16);
		f_clone_spec.add(f_clone_spec, clone_spec);
	}
};

REGISTER_PRIMITIVE(clone_ingress_pkt_to_egress);

class clone_egress_pkt_to_egress: public ActionPrimitive<const Data &,
		const Data &> {
	void operator ()(const Data &clone_spec, const Data &field_list_id) {
		Field &f_clone_spec = get_field("standard_metadata.clone_spec");
		f_clone_spec.shift_left(field_list_id, 16);
		f_clone_spec.add(f_clone_spec, clone_spec);
	}
};

REGISTER_PRIMITIVE(clone_egress_pkt_to_egress);

class resubmit: public ActionPrimitive<const Data &> {
	void operator ()(const Data &field_list_id) {
		if (get_phv().has_field("intrinsic_metadata.resubmit_flag")) {
			get_phv().get_field("intrinsic_metadata.resubmit_flag").set(
					field_list_id);
		}
	}
};

REGISTER_PRIMITIVE(resubmit);

class recirculate: public ActionPrimitive<const Data &> {
	void operator ()(const Data &field_list_id) {
		if (get_phv().has_field("intrinsic_metadata.recirculate_flag")) {
			get_phv().get_field("intrinsic_metadata.recirculate_flag").set(
					field_list_id);
		}
	}
};

REGISTER_PRIMITIVE(recirculate);

class modify_field_with_hash_based_offset: public ActionPrimitive<Data &,
		const Data &, const NamedCalculation &, const Data &> {
	void operator ()(Data &dst, const Data &base, const NamedCalculation &hash,
			const Data &size) {
		uint64_t v = (hash.output(get_packet()) % size.get<uint64_t>())
				+ base.get<uint64_t>();
		dst.set(v);
	}
};

REGISTER_PRIMITIVE(modify_field_with_hash_based_offset);

class no_op: public ActionPrimitive<> {
	void operator ()() {
		// nothing
	}
};

REGISTER_PRIMITIVE(no_op);

class execute_meter: public ActionPrimitive<MeterArray &, const Data &, Field &> {
	void operator ()(MeterArray &meter_array, const Data &idx, Field &dst) {
		dst.set(meter_array.execute_meter(get_packet(), idx.get_uint()));
	}
};

REGISTER_PRIMITIVE(execute_meter);

class count: public ActionPrimitive<CounterArray &, const Data &> {
	void operator ()(CounterArray &counter_array, const Data &idx) {
		counter_array.get_counter(idx.get_uint()).increment_counter(
				get_packet());
	}
};

REGISTER_PRIMITIVE(count);

class register_read: public ActionPrimitive<Field &, const RegisterArray &,
		const Data &> {
	void operator ()(Field &dst, const RegisterArray &src, const Data &idx) {
		dst.set(src[idx.get_uint()]);
	}
};

REGISTER_PRIMITIVE(register_read);

class register_write: public ActionPrimitive<RegisterArray &, const Data &,
		const Data &> {
	void operator ()(RegisterArray &dst, const Data &idx, const Data &src) {
		dst[idx.get_uint()].set(src);
	}
};

REGISTER_PRIMITIVE(register_write);

class push: public ActionPrimitive<HeaderStack &, const Data &> {
	void operator ()(HeaderStack &stack, const Data &num) {
		stack.push_front(num.get_uint());
	}
};

REGISTER_PRIMITIVE(push);

class pop: public ActionPrimitive<HeaderStack &, const Data &> {
	void operator ()(HeaderStack &stack, const Data &num) {
		stack.pop_front(num.get_uint());
	}
};

REGISTER_PRIMITIVE(pop);

// I cannot name this "truncate" and register it with the usual
// REGISTER_PRIMITIVE macro, because of a name conflict:
//
// In file included from /usr/include/boost/config/stdlib/libstdcpp3.hpp:77:0,
//   from /usr/include/boost/config.hpp:44,
//   from /usr/include/boost/cstdint.hpp:36,
//   from /usr/include/boost/multiprecision/number.hpp:9,
//   from /usr/include/boost/multiprecision/gmp.hpp:9,
//   from ../../src/bm_sim/include/bm_sim/bignum.h:25,
//   from ../../src/bm_sim/include/bm_sim/data.h:32,
//   from ../../src/bm_sim/include/bm_sim/fields.h:28,
//   from ../../src/bm_sim/include/bm_sim/phv.h:34,
//   from ../../src/bm_sim/include/bm_sim/actions.h:34,
//   from primitives.cpp:21:
//     /usr/include/unistd.h:993:12: note: declared here
//     extern int truncate (const char *__file, __off_t __length)
class truncate_: public ActionPrimitive<const Data &> {
	void operator ()(const Data &truncated_length) {
		get_packet().truncate(truncated_length.get<size_t>());
	}
};

REGISTER_PRIMITIVE_W_NAME("truncate", truncate_);

// dummy function, which ensures that this unit is not discarded by the linker
// it is being called by the constructor of SimpleSwitch
// the previous alternative was to have all the primitives in a header file (the
// primitives could also be placed in simple_switch.cpp directly), but I need
// this dummy function if I want to keep the primitives in their own file
int import_primitives() {
	return 0;
}
