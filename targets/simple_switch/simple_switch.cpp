/* Copyright 2013-present Barefoot Networks, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Antonin Bas (antonin@barefootnetworks.com)
 *
 */

//changed
#include <iomanip>// for show int in hex
//#include <iostream>
//stream
#include <sstream>

//Learn
#include <boost/shared_ptr.hpp>
#include <bm/bm_apps/learn.h>
//#include <bm/Standard.h>
//#include <bm/bm_apps/learn.h>
//#include "bm/Standard.h"
#include "../thrift_src/gen-cpp/bm/Standard.h"

//#include <bm/SimplePre.h>
//#include <string>
#include <functional>
#include <memory>
//#include <thread>
#include <mutex>

#define UNUSED(x) (void)(x)

//using namespace bm_runtime;

//Thrift
#ifdef P4THRIFT
#include <p4thrift/protocol/TBinaryProtocol.h>
#include <p4thrift/transport/TSocket.h>
#include <p4thrift/transport/TTransportUtils.h>
#include <p4thrift/protocol/TMultiplexedProtocol.h>

namespace thrift_provider = p4::thrift;
#else
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>
#include <thrift/protocol/TMultiplexedProtocol.h>

namespace thrift_provider = apache::thrift;
#endif

//NULL
#include<cstddef>

//Thread Sleep
#include <chrono>
#include <thread>
//Thread Sleep

//Start Thread
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
//Thread

#include <bm/bm_sim/parser.h>
#include <bm/bm_sim/tables.h>
#include <bm/bm_sim/logger.h>

#include <unistd.h>

#include <iostream>
#include <fstream>
#include <string>

#include "simple_switch.h"

#include <mutex>

//#include <bm/bm_apps/nn.h>

using namespace boost::asio;
using namespace std;

using namespace bm;

namespace {
namespace runtime = bm_runtime::standard;

struct hash_ex {
	uint32_t operator()(const char *buf, size_t s) const {
		const int p = 16777619;
		int hash = 2166136261;

		for (size_t i = 0; i < s; i++)
			hash = (hash ^ buf[i]) * p;

		hash += hash << 13;
		hash ^= hash >> 7;
		hash += hash << 3;
		hash ^= hash >> 17;
		hash += hash << 5;
		return static_cast<uint32_t>(hash);
	}
};

struct bmv2_hash {
	uint64_t operator()(const char *buf, size_t s) const {
		return bm::hash::xxh64(buf, s);
	}
};

}  // namespace

// if REGISTER_HASH calls placed in the anonymous namespace, some compiler can
// give an unused variable warning
REGISTER_HASH(hash_ex);
REGISTER_HASH(bmv2_hash);

extern int import_primitives();

SimpleSwitch::SimpleSwitch(int max_port, bool enable_swap) :
		Switch(enable_swap), max_port(max_port), input_buffer(1024),
#ifdef SSWITCH_PRIORITY_QUEUEING_ON
				egress_buffers(max_port, nb_egress_threads,
						64, EgressThreadMapper(nb_egress_threads),
						SSWITCH_PRIORITY_QUEUEING_NB_QUEUES),
#else
				egress_buffers(max_port, nb_egress_threads, 64,
						EgressThreadMapper(nb_egress_threads)),
#endif
				output_buffer(128), pre(new McSimplePreLAG()), start(
						clock::now()) {

	add_component<McSimplePreLAG>(pre);

	add_required_field("standard_metadata", "ingress_port");
	add_required_field("standard_metadata", "packet_length");
	add_required_field("standard_metadata", "instance_type");
	add_required_field("standard_metadata", "egress_spec");
	add_required_field("standard_metadata", "clone_spec");

	force_arith_field("standard_metadata", "ingress_port");
	force_arith_field("standard_metadata", "packet_length");
	force_arith_field("standard_metadata", "instance_type");
	force_arith_field("standard_metadata", "egress_spec");
	force_arith_field("standard_metadata", "clone_spec");

	force_arith_field("queueing_metadata", "enq_timestamp");
	force_arith_field("queueing_metadata", "enq_qdepth");
	force_arith_field("queueing_metadata", "deq_timedelta");
	force_arith_field("queueing_metadata", "deq_qdepth");

	force_arith_field("intrinsic_metadata", "ingress_global_timestamp");
	force_arith_field("intrinsic_metadata", "lf_field_list");
	force_arith_field("intrinsic_metadata", "mcast_grp");
	force_arith_field("intrinsic_metadata", "resubmit_flag");
	force_arith_field("intrinsic_metadata", "egress_rid");
	force_arith_field("intrinsic_metadata", "recirculate_flag");
	force_arith_field("intrinsic_metadata", "switch_role");

	import_primitives();
//	SimpleSwitch::instance=&this;
}

#define PACKET_LENGTH_REG_IDX 0

int SimpleSwitch::receive(int port_num, const char *buffer, int len) {
	static int pkt_id = 0;

	// this is a good place to call this, because blocking this thread will not
	// block the processing of existing packet instances, which is a requirement
	if (do_swap() == 0) {
		check_queueing_metadata();
	}

	// we limit the packet buffer to original size + 512 bytes, which means we
	// cannot add more than 512 bytes of header data to the packet, which should
	// be more than enough
	auto packet = new_packet_ptr(port_num, pkt_id++, len,
			bm::PacketBuffer(len + 512, buffer, len));

	BMELOG(packet_in, *packet);

	PHV *phv = packet->get_phv();
	// many current P4 programs assume this
	// it is also part of the original P4 spec
	phv->reset_metadata();

	// setting standard metadata

	phv->get_field("standard_metadata.ingress_port").set(port_num);
	// using packet register 0 to store length, this register will be updated for
	// each add_header / remove_header primitive call
	packet->set_register(PACKET_LENGTH_REG_IDX, len);
	phv->get_field("standard_metadata.packet_length").set(len);
	Field &f_instance_type = phv->get_field("standard_metadata.instance_type");
	f_instance_type.set(PKT_INSTANCE_TYPE_NORMAL);

	int switch_role = 0;

	if (phv->has_field("intrinsic_metadata.switch_role")) {
		Field &f_switch_role = phv->get_field("intrinsic_metadata.switch_role");
		switch_role = f_switch_role.get_int();
		Logger::get()->trace("switch_role:");
		Logger::get()->trace(switch_role);
	}

	if (phv->has_field("intrinsic_metadata.ingress_global_timestamp")) {
		phv->get_field("intrinsic_metadata.ingress_global_timestamp").set(
				get_ts().count());
	}

	input_buffer.push_front(std::move(packet));
	return 0;
}
std::atomic<long> num_of_packet_current;
void SimpleSwitch::start_and_return() {
	check_queueing_metadata();
	num_of_packet_current = 0;

	std::thread t1(&SimpleSwitch::ingress_thread, this);
	t1.detach();
	for (size_t i = 0; i < nb_egress_threads; i++) {
		std::thread t2(&SimpleSwitch::egress_thread, this, i);
		t2.detach();
	}
	std::thread t3(&SimpleSwitch::transmit_thread, this);
	t3.detach();

//	std::thread t4(&SimpleSwitch::congestion_minus_thread, this);
//	t4.detach();

	std::thread t5(&SimpleSwitch::increment_thread, this);
	t5.detach();

}

void SimpleSwitch::reset_target_state() {
	bm::Logger::get()->debug("Resetting simple_switch target-specific state");
	get_component<McSimplePreLAG>()->reset_state();
}

int SimpleSwitch::set_egress_queue_depth(int port, const size_t depth_pkts) {
	egress_buffers.set_capacity(port, depth_pkts);
	return 0;
}

int SimpleSwitch::set_all_egress_queue_depths(const size_t depth_pkts) {
	for (int i = 0; i < max_port; i++) {
		set_egress_queue_depth(i, depth_pkts);
	}
	return 0;
}

int SimpleSwitch::set_egress_queue_rate(int port, const uint64_t rate_pps) {
	egress_buffers.set_rate(port, rate_pps);
	return 0;
}

int SimpleSwitch::set_all_egress_queue_rates(const uint64_t rate_pps) {
	for (int i = 0; i < max_port; i++) {
		set_egress_queue_rate(i, rate_pps);
	}
	return 0;
}

bool hasFirstPacket = false;
std::shared_ptr<Packet> firstPacket;
boost::shared_ptr<bm_runtime::standard::StandardClient> bm_client { nullptr };

void SimpleSwitch::setThriftPort(int portNumber) {

	UNUSED(portNumber);

	if (thriftPort != portNumber) {
		Logger::get()->trace("Thrift Port Diff. Change Drift port: ");
		Logger::get()->trace(portNumber);
		thriftPort = portNumber;

		//	namespace runtime = bm_runtime::standard;

		using thrift_provider::protocol::TProtocol;
		using thrift_provider::protocol::TBinaryProtocol;
		using thrift_provider::protocol::TMultiplexedProtocol;
		using thrift_provider::transport::TSocket;
		using thrift_provider::transport::TTransport;
		using thrift_provider::transport::TBufferedTransport;

		boost::shared_ptr<TTransport> tsocket(
				new TSocket("localhost", portNumber));
		boost::shared_ptr<TTransport> transport(
				new TBufferedTransport(tsocket));
		boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

		boost::shared_ptr<TMultiplexedProtocol> standard_protocol(
				new TMultiplexedProtocol(protocol, "standard"));

		bm_client = boost::shared_ptr<bm_runtime::standard::StandardClient>(
				new bm_runtime::standard::StandardClient(standard_protocol));
		assert(bm_client);
		transport->open();

	}
}
//#define (void)(egress)
//#define (void)(srcIp)
//#define (void)(srcPort)
//#define (void)(dstIp)
//#define (void)(dstPort)
//#define (void)(protocol)

//void SimpleSwitch::addEgressToForwardingTable(long egress, long srcIp,
//		long srcPort, long dstIp, long dstPort, long protocol) {

void SimpleSwitch::addEgressToForwardingTable(std::string egress,
		std::string srcIp, std::string srcPort, std::string dstIp,
		std::string dstPort, std::string protocol) {

	Logger::get()->trace("addEgressToForwardingTable");
	Logger::get()->trace(egress); //change it to that format

//	srcIp = std::regex_replace(srcIp, std::regex("\x"), " ");

//	boost::shared_ptr<bm_runtime::standard::StandardClient> bm_client { nullptr };
//	//	namespace runtime = bm_runtime::standard;
//
//	using thrift_provider::protocol::TProtocol;
//	using thrift_provider::protocol::TBinaryProtocol;
//	using thrift_provider::protocol::TMultiplexedProtocol;
//	using thrift_provider::transport::TSocket;
//	using thrift_provider::transport::TTransport;
//	using thrift_provider::transport::TBufferedTransport;
//
//	boost::shared_ptr<TTransport> tsocket(new TSocket("localhost", thriftPort));
//	boost::shared_ptr<TTransport> transport(new TBufferedTransport(tsocket));
//	boost::shared_ptr<TProtocol> tProtocol(new TBinaryProtocol(transport));
//
//	boost::shared_ptr<TMultiplexedProtocol> standard_protocol(
//			new TMultiplexedProtocol(tProtocol, "standard"));
//
//	bm_client = boost::shared_ptr<bm_runtime::standard::StandardClient>(
//			new bm_runtime::standard::StandardClient(standard_protocol));
//	assert(bm_client);
//	transport->open();

	Logger::get()->trace(srcIp);
	Logger::get()->trace(srcPort);
	Logger::get()->trace(dstIp);
	Logger::get()->trace(dstPort);
	Logger::get()->trace(protocol);

	Logger::get()->trace(srcIp);
	Logger::get()->trace(dstIp);
	Logger::get()->trace("Hardcode for compare:");
	Logger::get()->trace(std::string("\x0a\x00\x01\x04", 4));
	Logger::get()->trace(std::string("\x0a\x00\x04\x04", 4));

//	UNUSED(egress);
//	UNUSED(srcIp);
//	UNUSED(srcPort);
//	UNUSED(dstIp);
//	UNUSED(dstPort);
//	UNUSED(protocol);

//match srcIp
	Logger::get()->trace("set srcIP match");
	runtime::BmMatchParam match_param_srcIp;
	match_param_srcIp.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_srcIp;
//	match_param_exact_srcIp.key = std::string(srcIp, 4);
	match_param_exact_srcIp.key = srcIp;
	match_param_srcIp.__set_exact(match_param_exact_srcIp);

//match srcPort
	Logger::get()->trace("set srcPort match");
	runtime::BmMatchParam match_param_srcPort;
	match_param_srcPort.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_srcPort;
//	match_param_exact_srcPort.key = std::string(srcPort, 2);
	match_param_exact_srcPort.key = srcPort;
	match_param_srcPort.__set_exact(match_param_exact_srcPort);

	//match dstIp
	Logger::get()->trace("set dstIp match");
	runtime::BmMatchParam match_param_dstIp;
	match_param_dstIp.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_dstIp;
//	match_param_exact_dstIp.key = std::string(dstIp, 4);
	match_param_exact_dstIp.key = dstIp;
	match_param_dstIp.__set_exact(match_param_exact_dstIp);

	//match dstPort
	Logger::get()->trace("set dstPort match");
	runtime::BmMatchParam match_param_dstPort;
	match_param_dstPort.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_dstPort;
//	match_param_exact_dstPort.key = std::string(dstPort, 2);
	match_param_exact_dstPort.key = dstPort;
	match_param_dstPort.__set_exact(match_param_exact_dstPort);

	//match protocol
	Logger::get()->trace("set protocol match");
	runtime::BmMatchParam match_param_protocol;
	match_param_protocol.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_protocol;
//	match_param_exact_protocol.key = std::string(protocol, 1);
	match_param_exact_protocol.key = protocol;
	match_param_protocol.__set_exact(match_param_exact_protocol);

	runtime::BmAddEntryOptions entry_options;
//	egress_options.
//	std::vector<std::string> egress_action_data = { std::string(egress, 2) };
	std::vector<std::string> egress_action_data = { egress };

//As packet origin from host A to B
//entry we need for forwarding is from B to A
//invert src, dst

	try {
		bm_client->bm_mt_add_entry(0, "forwarding_table", { match_param_dstIp,
				match_param_dstPort, match_param_srcIp, match_param_srcPort,
				match_param_protocol }, "expeditus_forwarding",
				std::move(egress_action_data), entry_options);
	} catch (bm_runtime::standard::InvalidTableOperation e) {
		Logger::get()->trace(string("error: ") + string(e.what()));
	}
//	try {
//		bm_client->bm_mt_add_entry(0, "forwarding_table",
//				{ match_param_srcIp,match_param_srcPort, match_param_dstIp,match_param_dstPort,match_param_protocol}, "expeditus_forwarding",
//				std::move(egress_action_data), entry_options);

//		bm_client->bm_mt_add_entry(0, "forwarding_table",
//						{ match_param_exact_srcIp,match_param_exact_srcPort, match_param_exact_dstIp,match_param_exact_dstPort,match_param_exact_protocol}, "expeditus_forwarding",
//						std::move(egress_action_data), entry_options);

//	} catch (apache::thrift::TApplicationException e) {
//
//		Logger::get()->trace(string("error: ") + string(e.what()));
//	}

//	runtime::BmMatchParam match_param;
//	match_param.type = runtime::BmMatchParamType::type::EXACT;
////
//	runtime::BmMatchParamExact match_param_exact;
//	match_param_exact.key = std::string("\x00\x01", 2);
//
////	Logger::get()->trace("port: "+match_param_exact.key);
////	match_param_exact.key = std::string("");
//	match_param.__set_exact(match_param_exact);
////
//	runtime::BmAddEntryOptions options;
////
////	std::vector<std::string> my_action_data = { std::string("aabb000000") };
//	std::vector<std::string> my_action_data = { std::string(
//			"\x00\xaa\xbb\x00\x00\x00", 6) };
////	Logger::get()->trace("my_action_data: "+std::string("\x00\xaa\xbb\x00\x00\x00",6));
////	std::vector<std::string> my_action_data = { std::string("",0) };
////	std::vector<std::string> my_action_data = { std::string(
////						"000100000101") };
//
////		bm_client->bm_mt_set_default_action()
////		bm_client->bm_mt_set_default_action(0,"send_frame",)
////		bm_client->bm_counter_write()
////		bm_client->bm_mt_add_entry(0, "send_frame", { match_param },
////				"rewrite_mac", std::move(my_action_data), options);
//
//	try {
//		bm_client->bm_mt_add_entry(0, "send_frame", { match_param }, "_nop",
//				std::vector<std::string>(), options);
//	} catch (apache::thrift::TApplicationException e) {
//
//		Logger::get()->trace(string("error: ")+string(e.what()));
//	}
}

void SimpleSwitch::congestion_minus_thread() {
	std::this_thread::sleep_for(std::chrono::milliseconds(4000));
//	std::unique_ptr<Packet> firstPacket(new Packet);

//	boost::shared_ptr<bm_runtime::standard::StandardClient> bm_client { nullptr };
//	namespace runtime = bm_runtime::standard;

//	using thrift_provider::protocol::TProtocol;
//	using thrift_provider::protocol::TBinaryProtocol;
//	using thrift_provider::protocol::TMultiplexedProtocol;
//	using thrift_provider::transport::TSocket;
//	using thrift_provider::transport::TTransport;
//	using thrift_provider::transport::TBufferedTransport;

//	boost::shared_ptr<TTransport> tsocket(new TSocket("localhost", 9090));
//	boost::shared_ptr<TTransport> transport(new TBufferedTransport(tsocket));
//	boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
//
//	boost::shared_ptr<TMultiplexedProtocol> standard_protocol(
//			new TMultiplexedProtocol(protocol, "standard"));
//
//	bm_client = boost::shared_ptr<bm_runtime::standard::StandardClient>(
//			new bm_runtime::standard::StandardClient(standard_protocol));
//	assert(bm_client);
//	transport->open();
//
//	runtime::BmMatchParam match_param;
//	match_param.type = runtime::BmMatchParamType::type::EXACT;
////
//	runtime::BmMatchParamExact match_param_exact;
//	match_param_exact.key = std::string("\x00\x01",2);
//
//	Logger::get()->trace("port: "+match_param_exact.key);
////	match_param_exact.key = std::string("");
//	match_param.__set_exact(match_param_exact);
////
//	runtime::BmAddEntryOptions options;
////
////	std::vector<std::string> my_action_data = { std::string("aabb000000") };
//	std::vector<std::string> my_action_data = { std::string("\x00\xaa\xbb\x00\x00\x00",6) };
//	Logger::get()->trace("my_action_data: "+std::string("\x00\xaa\xbb\x00\x00\x00",6));
////	std::vector<std::string> my_action_data = { std::string("",0) };
////	std::vector<std::string> my_action_data = { std::string(
////						"000100000101") };
//
////		bm_client->bm_mt_set_default_action()
////		bm_client->bm_mt_set_default_action(0,"send_frame",)
//		bm_client->bm_mt_add_entry(0, "send_frame", { match_param },
//				"rewrite_mac", std::move(my_action_data), options);

//		//Connect to thrift
//		    boost::shared_ptr<TTransport> tsocket(new TSocket("localhost", 9090));
//		    boost::shared_ptr<TTransport> transport(new TBufferedTransport(tsocket));
//		    boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
//		    boost::shared_ptr<TMultiplexedProtocol> standard_protocol(
//		            new TMultiplexedProtocol(protocol, "standard"));
//		    bm_client = boost::shared_ptr<bm_runtime::standard::StandardClient>(
//		            new bm_runtime::standard::StandardClient(standard_protocol));
//		    assert(bm_client);
//		    transport->open();

//		//Set table entries data
//		    runtime::BmMatchParam match_param;
//		    match_param.type = runtime::BmMatchParamType::type::EXACT;
//		    runtime::BmMatchParamExact match_param_exact;
////		    match_param_exact.key = std::string("\x00\x01",2);
//		    match_param_exact.key = std::string("\x0a\x00\x01\x04",4);
//
//		    match_param.__set_exact(match_param_exact);
//		    runtime::BmAddEntryOptions options;
//		    std::vector<std::string> my_action_data = { std::string("\x00\xaa\xbb\x00\x00\x00",6) };
//		//Add to entry
////		    bm_client->bm_mt_add_entry(0, "send_frame", { match_param },"rewrite_mac", std::move(my_action_data), options);
//		    		    bm_client->bm_mt_add_entry(0, "forward", { match_param },"set_dmac", std::move(my_action_data), options);

	setThriftPort(9090);

	//Hard code add forwarding table entry at run time
	//match srcIp
	Logger::get()->trace("set srcIP match");
	runtime::BmMatchParam match_param_srcIp;
	match_param_srcIp.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_srcIp;
	match_param_exact_srcIp.key = std::string("\x0a\x00\x01\x04", 4);
	match_param_srcIp.__set_exact(match_param_exact_srcIp);

	//match srcPort
	Logger::get()->trace("set srcPort match");
	runtime::BmMatchParam match_param_srcPort;
	match_param_srcPort.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_srcPort;
	match_param_exact_srcPort.key = std::string("\x00\x01", 2);
	match_param_srcPort.__set_exact(match_param_exact_srcPort);

	//match dstIp
	Logger::get()->trace("set dstIp match");
	runtime::BmMatchParam match_param_dstIp;
	match_param_dstIp.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_dstIp;
	match_param_exact_dstIp.key = std::string("\x0a\x00\x04\x04", 4);
	match_param_dstIp.__set_exact(match_param_exact_dstIp);

	//match dstPort
	Logger::get()->trace("set dstPort match");
	runtime::BmMatchParam match_param_dstPort;
	match_param_dstPort.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_dstPort;
	match_param_exact_dstPort.key = std::string("\x00\x02", 2);
	match_param_dstPort.__set_exact(match_param_exact_dstPort);

	//match protocol
	Logger::get()->trace("set protocol match");
	runtime::BmMatchParam match_param_protocol;
	match_param_protocol.type = runtime::BmMatchParamType::type::EXACT;
	runtime::BmMatchParamExact match_param_exact_protocol;
	match_param_exact_protocol.key = std::string("\x00\x06", 1);
	match_param_protocol.__set_exact(match_param_exact_protocol);

	runtime::BmAddEntryOptions entry_options;
	//	egress_options.
	std::vector<std::string> egress_action_data = { std::string("\x00\x02", 2) };

	//
	bm_client->bm_mt_add_entry(0, "forwarding_table", { match_param_srcIp,
			match_param_srcPort, match_param_dstIp, match_param_dstPort,
			match_param_protocol }, "expeditus_forwarding",
			std::move(egress_action_data), entry_options);

//	try {
//		bm_client->bm_mt_add_entry(0, "send_frame", { match_param }, "_nop",
//				std::vector<std::string>(), options);
//	} catch (apache::thrift::TApplicationException e) {

//		Logger::get()->trace(string("error: ")+string(e.what()));
//	}

//
////	std::this_thread::sleep_for(std::chrono::milliseconds(4000));
////	std::unique_ptr<Packet> firstPacket(new Packet);
//
//	boost::shared_ptr<bm_runtime::standard::StandardClient> bm_client { nullptr };
////	namespace runtime = bm_runtime::standard;
//
//	using thrift_provider::protocol::TProtocol;
//	using thrift_provider::protocol::TBinaryProtocol;
//	using thrift_provider::protocol::TMultiplexedProtocol;
//	using thrift_provider::transport::TSocket;
//	using thrift_provider::transport::TTransport;
//	using thrift_provider::transport::TBufferedTransport;
//
//	boost::shared_ptr<TTransport> tsocket(new TSocket("localhost", 9090));
//	boost::shared_ptr<TTransport> transport(new TBufferedTransport(tsocket));
//	boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));
//
//	boost::shared_ptr<TMultiplexedProtocol> standard_protocol(
//			new TMultiplexedProtocol(protocol, "standard"));
//
//	bm_client = boost::shared_ptr<bm_runtime::standard::StandardClient>(
//			new bm_runtime::standard::StandardClient(standard_protocol));
//	assert(bm_client);
//	transport->open();
//
//	runtime::BmMatchParam match_param;
//	match_param.type = runtime::BmMatchParamType::type::EXACT;
////
//	runtime::BmMatchParamExact match_param_exact;
//	match_param_exact.key = std::string("\x00\x01", 2);
//
////	Logger::get()->trace("port: "+match_param_exact.key);
////	match_param_exact.key = std::string("");
//	match_param.__set_exact(match_param_exact);
////
//	runtime::BmAddEntryOptions options;
////
////	std::vector<std::string> my_action_data = { std::string("aabb000000") };
//	std::vector<std::string> my_action_data = { std::string(
//			"\x00\xaa\xbb\x00\x00\x00", 6) };
////	Logger::get()->trace("my_action_data: "+std::string("\x00\xaa\xbb\x00\x00\x00",6));
////	std::vector<std::string> my_action_data = { std::string("",0) };
////	std::vector<std::string> my_action_data = { std::string(
////						"000100000101") };
//
////		bm_client->bm_mt_set_default_action()
////		bm_client->bm_mt_set_default_action(0,"send_frame",)
////		bm_client->bm_counter_write()
////		bm_client->bm_mt_add_entry(0, "send_frame", { match_param },
////				"rewrite_mac", std::move(my_action_data), options);
//
//	try {
//		bm_client->bm_mt_add_entry(0, "send_frame", { match_param }, "_nop",
//				std::vector<std::string>(), options);
//	} catch (apache::thrift::TApplicationException e) {
//
//		Logger::get()->trace(string("error: ")+string(e.what()));
//	}

//
//	while (1) {
//
////	     std::vector<std::string> action_data =
////	       {std::string(reinterpret_cast<const char *>(&sample->ingress_port), 2)};
////
////	     bm_client->bm_mt_add_entry(0, "dmac", {match_param},
////	                             "forward", std::move(action_data),
////	                             options);
////
//
////	     bm_client->bm_mt_add_entry()
////	     bm_client->bm_mt_indirect_ws_create_group()
//
//		Logger::get()->trace("repeat\n");
//		Logger::get()->trace(
//				"current# packet: " + std::to_string(num_of_packet_current));
//		num_of_packet_current = 0;
//
////		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
//		bool sleep = true;
//
//		auto startTime = std::chrono::high_resolution_clock::now();
//		while (sleep) {
//			auto now = std::chrono::high_resolution_clock::now();
////		    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - startTime);
//			auto elapsed = now - startTime;
//			if (elapsed.count() > 1000000000) {
//				sleep = false;
//				startTime = std::chrono::high_resolution_clock::now();
//			}
//		}
//
////    transmit_fn(packet->get_egress_port(),
////                packet->data(), packet->get_data_size());
//
//	}
}

long maxEWMAValue = 0;
std::mutex g_lock;
void SimpleSwitch::increment_thread() {
//	for(int i=0;i<MAX_PORT;i++){
//		portCongestionData[i]=0;
//	}

	count = 0;
//	long counter=0;
	auto startTime = std::chrono::high_resolution_clock::now();
	auto displayStart = std::chrono::high_resolution_clock::now();
	while (1) {
//
//		bool displaySleep = true;
//		while (displaySleep) {
//		bool sleep = true;

//		while (sleep) {
		auto now = std::chrono::high_resolution_clock::now();
//		    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(now - startTime);
		auto elapsed = now - startTime;
		long interval = elapsed.count();
//			if (interval > 10000000) {
		double currentEWMAValue[MAX_PORT];
		if (interval >= 1000000) {	//1000 nano * 100 =100000 = 100 micro
						//1000 nano = 1 micro
						

			//===========This should be moved to 100us
			//update EWMA
//			double currentEWMAValue[MAX_PORT];
			int times=interval/100000;

			for (int i = 0; i < MAX_PORT; i++) {
				
				for(int j=0;j<times;j++){
				g_lock.lock();
				currentEWMAValue[i] = (double)ewmaWeight * ((double)ewmaCountValues[i]/times)
						+ ((double)1 - (double)ewmaWeight) * (double)lastEWMAValues[i];
				lastEWMAValues[i]=currentEWMAValue[i];

//				ewmaCountValues[i] =0;
				g_lock.unlock();
				}
				ewmaCountValues[i]=0;
//				lastEWMAValues[i] = currentEWMAValue[i];

				if (currentEWMAValue[i] > maxEWMAValue) {
					maxEWMAValue = currentEWMAValue[i];
				}
			}

			//===================



			////////////show port 1 egress data and ewma
//			Logger::get()->trace(portCongestionData[1]);
//			Logger::get()->trace(count);
//
//			std::ostringstream stream;
//			stream << "ewmaEgressByteValues: " << ewmaCountValues[1];
//
//			Logger::get()->trace(stream.str());
			//////////////////

			/////////////////calculate 0-15
			/*tested with Experiment
			 * send with highest speed
			 * see max E
			 */
			const double MAX_CD_VALUE = 15;	//max value 15, range 0-15
//			double maxTrafficPer100us = 166666;//byte per 100 us (eg 1000byte *100 us)

			for (int i = 1; i < MAX_PORT; i++) { //for every port, traffic bits--> Congestion Data
				if(currentEWMAValue[i]>0){
					Logger::get()->trace("currentEWMAValue");
					Logger::get()->trace(i);
					Logger::get()->trace(currentEWMAValue[i] );
				}
				congestionDataForTransmit[i] = currentEWMAValue[i]
						/ 12000 * MAX_CD_VALUE;

				if (congestionDataForTransmit[i] > MAX_CD_VALUE) {
					congestionDataForTransmit[i] = MAX_CD_VALUE;
				}
			}

			//////////////////////

			////////////////show port 1 CD  (0-15)
//			std::ostringstream currentEWMAValueStream;
//			currentEWMAValueStream << "currentEWMAValue: "
//					<< currentEWMAValue[1] << " ;  CD: "
//					<< congestionDataForTransmit[1];
//			Logger::get()->trace(currentEWMAValueStream.str());
			/////////////////////end move to 100us

//				Logger::get()->trace(interval);
//				sleep = false;
			startTime = std::chrono::high_resolution_clock::now();
//			for (int i = 0; i < interval / 100000; i++) {

			num_of_packet_current++;
			count = count + 1;

			//===========This should be moved to 100us
//			for (int i = 0; i < MAX_PORT; i++) {
//				ewmaCountValues[i] = 0;
//			}
			//===================
//				for(int i=0;i<MAX_PORT;i++){
//					portCongestionData[i]++;
//				}
//			}
		}
		auto displayNow = std::chrono::high_resolution_clock::now();

		auto displayInterval = displayNow - displayStart;
		if (displayInterval.count() >= 10000000000) {//10s
			Logger::get()->trace(count);
			std::ofstream outfile;
			outfile.open("maxEWMAValue"+std::to_string(thriftPort)+".txt",std::ios_base::app);
			outfile<<"currentEWMAValue[1]: "<<currentEWMAValue[1]<<"\n";
			outfile<<"currentEWMAValue[2]:"<<currentEWMAValue[2]<<"\n";
			outfile<<maxEWMAValue<<"\n";
			
			///////////////gether all port CD (should calculate on request(from P4))
//			unsigned long long finalTrasnmitCD = 0;
//			for (int i = 0; i < MAX_UPSTREAM; i++) { //for every port
//				finalTrasnmitCD <<= 4;
//				finalTrasnmitCD = finalTrasnmitCD
//						+ congestionDataForTransmit[i];
//			}
//
////			finalTrasnmitCD=255;
////			finalTrasnmitCD<<=4;
//			std::ostringstream CDStream;
//			CDStream << "finalTrasnmitCD: " << std::hex << finalTrasnmitCD;
//			Logger::get()->trace(CDStream.str());
			///////////////////////////////

			//////////////////show thrift port
			Logger::get()->trace("Thrift port:");
			Logger::get()->trace(thriftPort);

			Logger::get()->trace("maxEWMAValue:");
			Logger::get()->trace(maxEWMAValue);

			////////////////////////

			num_of_packet_current = 0;
			count = 0;

//			for(int i=0;i<MAX_PORT;i++){
//				portCongestionData[i]=0;
//			}
//				displaySleep = false;
			displayStart = std::chrono::high_resolution_clock::now();
		}

//		}

//		Logger::get()->trace("repeat\n");
//		Logger::get()->trace("current# packet: "+std::to_string(num_of_packet_current));
//		if (counter % 3 == 0) {

//		auto displayInterval = displayNow - displayStart;
//		if (displayInterval.count() > 1000000000) {
//			Logger::get()->trace(count);
//			num_of_packet_current = 0;
//			count = 0;
//			displaySleep = false;
//			displayStart = std::chrono::high_resolution_clock::now();
//		}
//		}
//		}
//		counter++;
//		std::this_thread::sleep_for(std::chrono::microseconds(1000));

//		std::this_thread::sleep_for(std::chrono::nanoseconds(1));
	}
}

void SimpleSwitch::transmit_thread() {
	while (1) {
		std::unique_ptr<Packet> packet;
		output_buffer.pop_back(&packet);
		BMELOG(packet_out, *packet);BMLOG_DEBUG_PKT(*packet,
				"Transmitting packet of size {} out of port {}",
				packet->get_data_size(), packet->get_egress_port());
//		portCongestionData[packet->get_egress_port()]++;
//		g_lock.lock();
		ewmaCountValues[packet->get_egress_port()] += (packet->get_data_size()
				* 8 + packet->get_payload_size() * 8);
//		g_lock.unlock();
		transmit_fn(packet->get_egress_port(), packet->data(),
				packet->get_data_size());
	}
}

ts_res SimpleSwitch::get_ts() const {
	return duration_cast<ts_res>(clock::now() - start);
}

void SimpleSwitch::enqueue(int egress_port, std::unique_ptr<Packet> &&packet) {
	packet->set_egress_port(egress_port);

	PHV *phv = packet->get_phv();

	if (with_queueing_metadata) {
		phv->get_field("queueing_metadata.enq_timestamp").set(get_ts().count());
		phv->get_field("queueing_metadata.enq_qdepth").set(
				egress_buffers.size(egress_port));
	}

#ifdef SSWITCH_PRIORITY_QUEUEING_ON
	size_t priority =
	phv->get_field(SSWITCH_PRIORITY_QUEUEING_SRC).get<size_t>();
	if (priority >= SSWITCH_PRIORITY_QUEUEING_NB_QUEUES) {
		bm::Logger::get()->error("Priority out of range, dropping packet");
		return;
	}
	egress_buffers.push_front(
			egress_port, SSWITCH_PRIORITY_QUEUEING_NB_QUEUES - 1 - priority,
			std::move(packet));
#else
	egress_buffers.push_front(egress_port, std::move(packet));
#endif
}

// used for ingress cloning, resubmit
std::unique_ptr<Packet> SimpleSwitch::copy_ingress_pkt(
		const std::unique_ptr<Packet> &packet, PktInstanceType copy_type,
		p4object_id_t field_list_id) {
	std::unique_ptr<Packet> packet_copy = packet->clone_no_phv_ptr();
	PHV *phv_copy = packet_copy->get_phv();
	phv_copy->reset_metadata();
	FieldList *field_list = this->get_field_list(field_list_id);
	const PHV *phv = packet->get_phv();
	for (const auto &p : *field_list) {
		phv_copy->get_field(p.header, p.offset).set(
				phv->get_field(p.header, p.offset));
	}
	phv_copy->get_field("standard_metadata.instance_type").set(copy_type);
	return packet_copy;
}

void SimpleSwitch::check_queueing_metadata() {
	bool enq_timestamp_e = field_exists("queueing_metadata", "enq_timestamp");
	bool enq_qdepth_e = field_exists("queueing_metadata", "enq_qdepth");
	bool deq_timedelta_e = field_exists("queueing_metadata", "deq_timedelta");
	bool deq_qdepth_e = field_exists("queueing_metadata", "deq_qdepth");
	if (enq_timestamp_e || enq_qdepth_e || deq_timedelta_e || deq_qdepth_e) {
		if (enq_timestamp_e && enq_qdepth_e && deq_timedelta_e && deq_qdepth_e)
			with_queueing_metadata = true;
		else
			bm::Logger::get()->warn(
					"Your JSON input defines some but not all queueing metadata fields");
	}
}

void SimpleSwitch::ingress_thread() {
	PHV *phv;

	while (1) {
		std::unique_ptr<Packet> packet;
		input_buffer.pop_back(&packet);

//		num_of_packet_current++;
//    hasFirstPacket=true;
//    firstPacket=packet;
//    firstPacket=std::move(packet);

		// TODO(antonin): only update these if swapping actually happened?
		Parser *parser = this->get_parser("parser");
		Pipeline *ingress_mau = this->get_pipeline("ingress");

		phv = packet->get_phv();

		int ingress_port = packet->get_ingress_port();
		(void) ingress_port;
		BMLOG_DEBUG_PKT(*packet, "Processing packet received on port {}",
				ingress_port);

		/* This looks like it comes out of the blue. However this is needed for
		 ingress cloning. The parser updates the buffer state (pops the parsed
		 headers) to make the deparser's job easier (the same buffer is
		 re-used). But for ingress cloning, the original packet is needed. This
		 kind of looks hacky though. Maybe a better solution would be to have the
		 parser leave the buffer unchanged, and move the pop logic to the
		 deparser. TODO? */
		const Packet::buffer_state_t packet_in_state =
				packet->save_buffer_state();
		parser->parse(packet.get());

		ingress_mau->apply(packet.get());

		packet->reset_exit();

		Field &f_egress_spec = phv->get_field("standard_metadata.egress_spec");
		int egress_spec = f_egress_spec.get_int();

		Field &f_clone_spec = phv->get_field("standard_metadata.clone_spec");
		unsigned int clone_spec = f_clone_spec.get_uint();

		int learn_id = 0;
		unsigned int mgid = 0u;

		if (phv->has_field("intrinsic_metadata.lf_field_list")) {
			Field &f_learn_id = phv->get_field(
					"intrinsic_metadata.lf_field_list");
			learn_id = f_learn_id.get_int();
		}

		// detect mcast support, if this is true we assume that other fields needed
		// for mcast are also defined
		if (phv->has_field("intrinsic_metadata.mcast_grp")) {
			Field &f_mgid = phv->get_field("intrinsic_metadata.mcast_grp");
			mgid = f_mgid.get_uint();
		}

		int egress_port;

		// INGRESS CLONING
		if (clone_spec) {
			BMLOG_DEBUG_PKT(*packet, "Cloning packet at ingress");
			egress_port = get_mirroring_mapping(clone_spec & 0xFFFF);
			f_clone_spec.set(0);
			if (egress_port >= 0) {
				const Packet::buffer_state_t packet_out_state =
						packet->save_buffer_state();
				packet->restore_buffer_state(packet_in_state);
				p4object_id_t field_list_id = clone_spec >> 16;
				auto packet_copy = copy_ingress_pkt(packet,
						PKT_INSTANCE_TYPE_INGRESS_CLONE, field_list_id);
				// we need to parse again
				// the alternative would be to pay the (huge) price of PHV copy for
				// every ingress packet
				parser->parse(packet_copy.get());
				enqueue(egress_port, std::move(packet_copy));
				packet->restore_buffer_state(packet_out_state);
			}
		}

		// LEARNING
		if (learn_id > 0) {
			get_learn_engine()->learn(learn_id, *packet.get());
		}

		// RESUBMIT
		if (phv->has_field("intrinsic_metadata.resubmit_flag")) {
			Field &f_resubmit = phv->get_field(
					"intrinsic_metadata.resubmit_flag");
			if (f_resubmit.get_int()) {
				BMLOG_DEBUG_PKT(*packet, "Resubmitting packet");
				// get the packet ready for being parsed again at the beginning of
				// ingress
				packet->restore_buffer_state(packet_in_state);
				p4object_id_t field_list_id = f_resubmit.get_int();
				f_resubmit.set(0);
				// TODO(antonin): a copy is not needed here, but I don't yet have an
				// optimized way of doing this
				auto packet_copy = copy_ingress_pkt(packet,
						PKT_INSTANCE_TYPE_RESUBMIT, field_list_id);
				input_buffer.push_front(std::move(packet_copy));
				continue;
			}
		}

		Field &f_instance_type = phv->get_field(
				"standard_metadata.instance_type");

		// MULTICAST
		int instance_type = f_instance_type.get_int();
		if (mgid != 0) {
			BMLOG_DEBUG_PKT(*packet, "Multicast requested for packet");
			Field &f_rid = phv->get_field("intrinsic_metadata.egress_rid");
			const auto pre_out = pre->replicate( { mgid });
			auto packet_size = packet->get_register(PACKET_LENGTH_REG_IDX);
			for (const auto &out : pre_out) {
				egress_port = out.egress_port;
				// if (ingress_port == egress_port) continue; // pruning
				BMLOG_DEBUG_PKT(*packet, "Replicating packet on port {}",
						egress_port);
				f_rid.set(out.rid);
				f_instance_type.set(PKT_INSTANCE_TYPE_REPLICATION);
				std::unique_ptr<Packet> packet_copy =
						packet->clone_with_phv_ptr();
				packet_copy->set_register(PACKET_LENGTH_REG_IDX, packet_size);
				enqueue(egress_port, std::move(packet_copy));
			}
			f_instance_type.set(instance_type);

			// when doing multicast, we discard the original packet
			continue;
		}

		egress_port = egress_spec;
		BMLOG_DEBUG_PKT(*packet, "Egress port is {}", egress_port);

		if (egress_port == 511) {  // drop packet
			BMLOG_DEBUG_PKT(*packet, "Dropping packet at the end of ingress");
			continue;
		}

		enqueue(egress_port, std::move(packet));
	}
}

void SimpleSwitch::egress_thread(size_t worker_id) {
	PHV *phv;

	while (1) {

		std::unique_ptr<Packet> packet;
		size_t port;
		egress_buffers.pop_back(worker_id, &port, &packet);

		Deparser *deparser = this->get_deparser("deparser");
		Pipeline *egress_mau = this->get_pipeline("egress");

		phv = packet->get_phv();

		if (with_queueing_metadata) {
			auto enq_timestamp = phv->get_field(
					"queueing_metadata.enq_timestamp").get<ts_res::rep>();
			phv->get_field("queueing_metadata.deq_timedelta").set(
					get_ts().count() - enq_timestamp);
			phv->get_field("queueing_metadata.deq_qdepth").set(
					egress_buffers.size(port));
		}

		phv->get_field("standard_metadata.egress_port").set(port);

		Field &f_egress_spec = phv->get_field("standard_metadata.egress_spec");
		f_egress_spec.set(0);

		phv->get_field("standard_metadata.packet_length").set(
				packet->get_register(PACKET_LENGTH_REG_IDX));

		egress_mau->apply(packet.get());

		Field &f_clone_spec = phv->get_field("standard_metadata.clone_spec");
		unsigned int clone_spec = f_clone_spec.get_uint();

		// EGRESS CLONING
		if (clone_spec) {
			BMLOG_DEBUG_PKT(*packet, "Cloning packet at egress");
			int egress_port = get_mirroring_mapping(clone_spec & 0xFFFF);
			if (egress_port >= 0) {
				f_clone_spec.set(0);
				p4object_id_t field_list_id = clone_spec >> 16;
				std::unique_ptr<Packet> packet_copy =
						packet->clone_with_phv_reset_metadata_ptr();
				PHV *phv_copy = packet_copy->get_phv();
				FieldList *field_list = this->get_field_list(field_list_id);
				for (const auto &p : *field_list) {
					phv_copy->get_field(p.header, p.offset).set(
							phv->get_field(p.header, p.offset));
				}
				phv_copy->get_field("standard_metadata.instance_type").set(
						PKT_INSTANCE_TYPE_EGRESS_CLONE);
				enqueue(egress_port, std::move(packet_copy));
			}
		}

		// TODO(antonin): should not be done like this in egress pipeline
		int egress_spec = f_egress_spec.get_int();
		if (egress_spec == 511) {  // drop packet
			BMLOG_DEBUG_PKT(*packet, "Dropping packet at the end of egress");
			continue;
		}

		deparser->deparse(packet.get());

		// RECIRCULATE
		if (phv->has_field("intrinsic_metadata.recirculate_flag")) {
			Field &f_recirc = phv->get_field(
					"intrinsic_metadata.recirculate_flag");
			if (f_recirc.get_int()) {
				BMLOG_DEBUG_PKT(*packet, "Recirculating packet");
				p4object_id_t field_list_id = f_recirc.get_int();
				f_recirc.set(0);
				FieldList *field_list = this->get_field_list(field_list_id);
				// TODO(antonin): just like for resubmit, there is no need for a copy
				// here, but it is more convenient for this first prototype
				std::unique_ptr<Packet> packet_copy =
						packet->clone_no_phv_ptr();
				PHV *phv_copy = packet_copy->get_phv();
				phv_copy->reset_metadata();
				for (const auto &p : *field_list) {
					phv_copy->get_field(p.header, p.offset).set(
							phv->get_field(p.header, p.offset));
				}
				phv_copy->get_field("standard_metadata.instance_type").set(
						PKT_INSTANCE_TYPE_RECIRC);
				size_t packet_size = packet_copy->get_data_size();
				packet_copy->set_register(PACKET_LENGTH_REG_IDX, packet_size);
				phv_copy->get_field("standard_metadata.packet_length").set(
						packet_size);
				input_buffer.push_front(std::move(packet_copy));
				continue;
			}
		}

		output_buffer.push_front(std::move(packet));
	}
}
