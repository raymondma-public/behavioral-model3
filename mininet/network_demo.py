#!/usr/bin/env python2

# Copyright 2013-present Barefoot Networks, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.log import setLogLevel, info
from mininet.cli import CLI
#from mininet.node import CPULimitedHost
from mininet.link import TCLink

from p4_mininet import P4Switch, P4Host

import argparse
from time import sleep

parser = argparse.ArgumentParser(description='Mininet demo')
parser.add_argument('--behavioral-exe', help='Path to behavioral executable',
					type=str, action="store", required=True)
parser.add_argument('--thrift-port', help='Thrift server port for table updates',
					type=int, action="store", default=9090)
parser.add_argument('--num-hosts', help='Number of hosts to connect to switch',
					type=int, action="store", default=2)
parser.add_argument('--mode', choices=['l2', 'l3'], type=str, default='l3')
parser.add_argument('--json', help='Path to JSON config file',
					type=str, action="store", required=True)
parser.add_argument('--pcap-dump', help='Dump packets on interfaces to pcap files',
					type=str, action="store", required=False, default=False)

parser.add_argument('--num-core-plane', help='number of core plane',
					type=int, action="store", required=False, default=2)
parser.add_argument('--num-core-switch-in-plane', help='number of core switch in 1 plane',
					type=int, action="store", required=False, default=2)
parser.add_argument('--num-tor-in-plane', help='number of ToR switch in 1 plane',
					type=int, action="store", required=False, default=1)

args = parser.parse_args()

MAX_PORT=255

class ClosTopo(Topo):
	"Single switch connected to n (< 256) hosts."
	def __init__(self, sw_path, json_path, thrift_port, pcap_dump,n_hosts, n_core_plane,n_core_switch_in_plane,n_tor_in_plane, **opts):
		# Initialize topology and default options
		Topo.__init__(self, **opts)

		totalNumSwitch=0
		n=n_hosts #num host
		num_switch_in_core=n_core_switch_in_plane
		num_aggregate_switch=n_core_plane
		print(str(thrift_port)+"\n");



		current_core_plane_number=0
		current_core_switch_number=0
		current_aggregate_switch_number=0
		

		list_of_aggregate_list=[]
		list_of_core_list=[]
		list_of_tor_list=[]


		h_count=0;
		for i in xrange(n_core_plane): #for each of the core plane
			current_core_plane_number=i+1

			list_of_core=[]
			list_of_core_list.append(list_of_core)

			list_of_aggregate=[]
			list_of_aggregate_list.append(list_of_aggregate)

			list_of_tor=[]
			list_of_tor_list.append(list_of_tor)
			for j in xrange(n_core_switch_in_plane): #some core switch
				current_core_switch_number=j+1

				print(str(thrift_port+totalNumSwitch))
				core_s_name="sc"+str(current_core_plane_number)+"_"+str(current_core_switch_number)
				core_s=		switch = self.addSwitch(core_s_name,
								sw_path = sw_path,
								json_path = json_path,
								thrift_port = (thrift_port+totalNumSwitch),
								pcap_dump = pcap_dump)
				list_of_core.append(core_s)
				print(core_s)
				f=open('run_as_'+str(thrift_port+totalNumSwitch)+'.sh','w')
				f.write('#!/bin/sh\n')
				f.write('THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )\n')
				f.write('source $THIS_DIR/env.sh\n')
				f.write('SWITCH_PATH="../simple_switch/simple_switch"\n')

				f.write('CLI_PATH="../simple_switch/sswitch_CLI"\n')
				f.write('set -m\n')
				f.write('if [ $? -ne 0]; then\n')
				f.write('echo "p4 compilation failed"\n')
				f.write('exit 1\n')
				f.write('fi\n')
				f.write('sudo $SWITCH_PATH >/dev/null 2>&1\n')
				f.write('sudo $SWITCH_PATH solution_2/simple_router.json \\ \n')
				f.write('-i 1@eth6 -i 2@eth4 &\n')
				f.write('sleep 2\n')
				f.write('echo "*** Send command to switch ***"\n')
				f.write('$CLI_PATH solution_2/simple_router.json < ./'+str(thrift_port+totalNumSwitch)+'.txt\n')
				f.write('echo "READY!!!"\n')
				f.write('fg\n')
				f.close()
				totalNumSwitch+=1

			for j in xrange(num_aggregate_switch): #some aggregate switch
				current_aggregate_switch_number=j+1
				print(str(thrift_port+totalNumSwitch))
				aggregate_s_name="sa"+str(current_core_plane_number)+"_"+str(current_aggregate_switch_number)
				aggregate_s=	 switch = self.addSwitch(aggregate_s_name,
								sw_path = sw_path,
								json_path = json_path,
								thrift_port = (thrift_port+totalNumSwitch),
								pcap_dump = pcap_dump)
				list_of_aggregate.append(aggregate_s)
				print(aggregate_s)
				
				f=open('run_as_'+str(thrift_port+totalNumSwitch)+'.sh','w')
				f.write('#!/bin/sh\n')
				f.write('THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )\n')
				f.write('source $THIS_DIR/env.sh\n')
				f.write('SWITCH_PATH="../simple_switch/simple_switch"\n')

				f.write('CLI_PATH="../simple_switch/sswitch_CLI"\n')
				f.write('set -m\n')
				f.write('if [ $? -ne 0]; then\n')
				f.write('echo "p4 compilation failed"\n')
				f.write('exit 1\n')
				f.write('fi\n')
				f.write('sudo $SWITCH_PATH >/dev/null 2>&1\n')
				f.write('sudo $SWITCH_PATH solution_2/simple_router.json \\ \n')
				f.write('-i 1@eth6 -i 2@eth4 &\n')
				f.write('sleep 2\n')
				f.write('echo "*** Send command to switch ***"\n')
				f.write('$CLI_PATH solution_2/simple_router.json < ./'+str(thrift_port+totalNumSwitch)+'.txt\n')
				f.write('echo "READY!!!"\n')
				f.write('fg\n')
				f.close()

				totalNumSwitch+=1

			# current_tor_index=0
			for j in xrange(n_tor_in_plane): #some tor switch
				current_tor_index=j+1

				print(str(thrift_port+totalNumSwitch))
				tor_s_name="st"+str(current_core_plane_number)+"_"+str(current_tor_index)
				tor_s=switch = self.addSwitch(tor_s_name,
								sw_path = sw_path,
								json_path = json_path,
								thrift_port = (thrift_port+totalNumSwitch),
								pcap_dump = pcap_dump)
				for k in xrange(n):
					host_index=k+1
					host_name="h"+str(current_core_plane_number)+"_"+str(current_tor_index)+"_"+str(host_index)
					h_count+=1
					print("host: "+str(h_count))
					host = self.addHost(host_name,
									ip = "10.0.%d.4/24" % h_count,
									mac = '00:04:00:00:%02x:00' %h_count
									)
					self.addLink(tor_s,host,MAX_PORT-host_index,0,bw=1000)# 1Mbps

				list_of_tor.append(tor_s)
				print(tor_s)
				
				f=open('run_as_'+str(thrift_port+totalNumSwitch)+'.sh','w')
				f.write('#!/bin/sh\n')
				f.write('THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}")" && pwd )\n')
				f.write('source $THIS_DIR/env.sh\n')
				f.write('SWITCH_PATH="../simple_switch/simple_switch"\n')

				f.write('CLI_PATH="../simple_switch/sswitch_CLI"\n')
				f.write('set -m\n')
				f.write('if [ $? -ne 0]; then\n')
				f.write('echo "p4 compilation failed"\n')
				f.write('exit 1\n')
				f.write('fi\n')
				f.write('sudo $SWITCH_PATH >/dev/null 2>&1\n')
				f.write('sudo $SWITCH_PATH solution_2/simple_router.json \\ \n')
				f.write('-i 1@eth6 -i 2@eth4 &\n')
				f.write('sleep 2\n')
				f.write('echo "*** Send command to switch ***"\n')
				f.write('$CLI_PATH solution_2/simple_router.json < ./'+str(thrift_port+totalNumSwitch)+'.txt\n')
				f.write('echo "READY!!!"\n')
				f.write('fg\n')
				f.close()

				totalNumSwitch+=1

		f = open('add_entries_from_py.sh','w')
		f.write('#!/bin/sh\n') 
		f.write('a=0\n') 
		f.write('port=9089\n') 
		f.write('while [ \"$a\" -lt '+str(totalNumSwitch)+' ]\n') 
		f.write('do\n') 
		f.write('\tport=`expr $port + 1 `\n') 
		f.write('\tpath="./$port.txt"\n') 
		f.write('\t\n') 
		f.write('\techo $path\n')
		# f.write('\t./runtime_CLI --thrift-port $port < $path\n')
		f.write('\t../simple_switch/sswitch_CLI --thrift-port $port < $path\n')
		# f.write('\tgnome-terminal -x sh -c \"./runtime_CLI --thrift-port $port < $path\"\n') 
		f.write('\ta=`expr $a + 1`\n') 
		f.write('done\n') 
		f.write('\n') 
		f.write('\n') 
		f.write('\n') 
		f.write('\n') 
		f.close() # you can omit in most cases as the destructor will call it

	
		print("core")
		for list in list_of_core_list:
			print ','.join(list)

		print("aggregate")
		for list in list_of_aggregate_list:
			print ','.join(list)

		print("tor")
		for list in list_of_tor_list:
			print ','.join(list)

		tor_index=0

		# #left Tor

		# print("st1")
		# totalNumSwitch+=1

		# #right Tor
		# print("st2")
		# totalNumSwitch+=1


		print("\n\nconnections")
		core_plane_index=0
		
		for core_list in list_of_core_list:
			core_plane_index+=1

			core_index=0;
			for core in core_list:
				core_index+=1

				aggregate_list_index=0
				for aggregate_list in list_of_aggregate_list:#[core_plane_index-1]
					aggregate_list_index+=1
					print ("- "+core+" "+aggregate_list[core_plane_index-1]+ " "+ str(aggregate_list_index) +" "+str(core_index))
					self.addLink(core, aggregate_list[core_plane_index-1],aggregate_list_index,core_index,bw=1000)


			aggregate_index=0
			tor_index=0
			for tor in list_of_tor_list[core_plane_index-1]:
				tor_index+=1
				aggregate_index=0
				for aggregate in list_of_aggregate_list[core_plane_index-1]:
					aggregate_index+=1
					print (tor+" "+aggregate+" "+str(aggregate_index)+" "+str(MAX_PORT-tor_index))
					self.addLink(tor, aggregate,aggregate_index,MAX_PORT-tor_index,bw=1000)




		print(str(thrift_port+totalNumSwitch)+"\n")
		print("totalNumSwitch"+str(totalNumSwitch))



		# switch = self.addSwitch('s1',
		#						 sw_path = sw_path,
		#						 json_path = json_path,
		#						 thrift_port = 9089,
		#						 pcap_dump = pcap_dump)


		# for h in xrange(n):
		#	 host = self.addHost('h%d' % (h + 1),
		#						 ip = "10.0.%d.10/24" % h,
		#						 mac = '00:04:00:00:00:%02x' %h)
		#	 self.addLink(host, switch) 

		# self.addLink("sa1_2", "sa2_1", 133, 133 )
		# self.addLink("st1_1", "st2_1", 133, 133 )


def main():
	num_hosts= args.num_hosts
	num_core_plane = args.num_core_plane
	num_core_switch_in_plane= args.num_core_switch_in_plane
	num_tor_in_plane= args.num_tor_in_plane
	num_aggregate_switch=num_core_plane

	mode = args.mode

	topo = ClosTopo(args.behavioral_exe,
							args.json,
							args.thrift_port,
							args.pcap_dump,
							num_hosts,
							num_core_plane,
							num_core_switch_in_plane,
							num_tor_in_plane
							)


	net = Mininet(topo = topo,
				  host = P4Host,
				  switch = P4Switch,
				  controller = None,
				  link=TCLink)
	net.start()


	sw_mac = ["00:aa:bb:00:00:%02x" % n for n in xrange(num_hosts)]

	sw_addr = ["10.0.%d.1" % n for n in xrange(num_hosts)]

	for i in xrange(num_core_plane):
		plane_index=i+1

		for j in xrange(num_core_switch_in_plane):
			aggregate_index=j+1

			index_string=str(plane_index)+'_'+str(aggregate_index)

			
			core_name='sc'+index_string
			# currentSwitch=net.get(core_name)
			# num_switch+=1
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
			# num_switch+=1
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')

			# num_switch+=1
			aggregate_name='sa'+index_string
			# currentSwitch=net.get(aggregate_name)
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
			# num_switch+=1
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')


		for j in xrange(num_tor_in_plane):
			tor_index=j+1

			# num_switch+=1
			tor_name='st'+str(plane_index)+'_'+str(tor_index)
			# currentSwitch=net.get(tor_name)
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
			# num_switch+=1
			# currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')


	# for n in xrange(num_hosts):
	#	 h = net.get('h%d' % (n + 1))
	#	 if mode == "l2":
	#		 h.setDefaultRoute("dev eth0")
	#	 else:
	#		 h.setARP(sw_addr[n], sw_mac[n])
	#		 h.setDefaultRoute("dev eth0 via %s" % sw_addr[n])

	# for n in xrange(num_hosts):
	#	 h = net.get('h%d' % (n + 1))
	#	 h.describe()



	for i in xrange(num_core_plane):
		plane_index=i+1
		for j in xrange(num_tor_in_plane):
			tor_index=j+1
			for k in xrange(num_hosts):
				host_index=k+1
				host_name="h"+str(plane_index)+"_"+str(tor_index)+"_"+str(host_index)
				h=net.get(host_name)
				if mode == "l2":
					h.setDefaultRoute("dev eth0")
				else:
					h.setARP(sw_addr[n], sw_mac[n])
					h.setDefaultRoute("dev eth0 via %s" % sw_addr[n])
				h.describe()

	switch_inf_map_ip={}

	total_num_core_switch_inf=0
	total_num_aggregate_inf=0
	total_num_tor_inf=0
	total_num_switch=0
	for i in xrange(num_core_plane):
		core_plane_index=i+1


		for j in xrange(num_core_switch_in_plane):
			core_switch_index=j+1

			core_name="sc%d_%d" % (core_plane_index,core_switch_index)
			mySwitch=net.get(core_name)
			print(core_name)
			for k in xrange (num_core_plane):
				total_num_core_switch_inf+=1
				total_num_switch+=1
				mySwitch.setMAC('00:0a:00:00:00:%02x' % total_num_core_switch_inf,intf=core_name+str("-eth%d" % (k+1)));
				inf_name=core_name+str("-eth%d" % (k+1))
				print("inf_name:"+inf_name+"\n\n\n")
				mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=inf_name)

				print(mySwitch.IP(intf=inf_name))
				switch_inf_map_ip[inf_name]=mySwitch.IP(intf=inf_name)
		for j in xrange(num_aggregate_switch):
			aggregate_index=j+1

			aggregate_name="sa%d_%d" % (core_plane_index,aggregate_index)
			mySwitch=net.get(aggregate_name)
			for k in xrange(num_core_switch_in_plane):
				total_num_aggregate_inf+=1
				total_num_switch+=1
				mySwitch.setMAC('00:0b:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (k+1)));
				mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (k+1)))
				switch_inf_map_ip[aggregate_name+str("-eth%d" % (k+1))]=mySwitch.IP(aggregate_name+str("-eth%d" % (k+1)))
				print(mySwitch.IP(aggregate_name+str("-eth%d" % (k+1))))

			for k in xrange(num_tor_in_plane):
				total_num_aggregate_inf+=1
				total_num_switch+=1
				mySwitch.setMAC('00:0b:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)));
				mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)))
				print(mySwitch.IP(aggregate_name+str("-eth%d" % (MAX_PORT-k-1))))
				switch_inf_map_ip[aggregate_name+str("-eth%d" % (MAX_PORT-k-1))]=mySwitch.IP(aggregate_name+str("-eth%d" % (MAX_PORT-k-1)))
		for j in xrange(num_tor_in_plane):
			aggregate_index=j+1

			aggregate_name="st%d_%d" % (core_plane_index,aggregate_index)
			mySwitch=net.get(aggregate_name)
			for k in xrange(num_core_plane):
				total_num_aggregate_inf+=1
				total_num_switch+=1
				mySwitch.setMAC('00:0c:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (k+1)));
				mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (k+1)))

				print(mySwitch.IP(aggregate_name+str("-eth%d" % (k+1))))
				switch_inf_map_ip[aggregate_name+str("-eth%d" % (k+1))]=mySwitch.IP(aggregate_name+str("-eth%d" % (k+1)))

			for k in xrange(num_hosts):
				total_num_aggregate_inf+=1
				total_num_switch+=1
				mySwitch.setMAC('00:0c:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)));
				# mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)))
				# print(mySwitch.IP(aggregate_name+str("-eth%d" % (MAX_PORT-k-1))))




	total_host=0
	for i in xrange(num_core_plane):
		core_plane_index=i+1
		aggregate_name="st%d_1"  % core_plane_index
		mySwitch=net.get(aggregate_name)

		for j in xrange(num_hosts):
			host_index=j+1
			total_host+=1
			mySwitch.setIP('10.0.%d.1/24' % total_host ,intf=aggregate_name+"-eth%d"%(MAX_PORT-host_index))


	for i in xrange(num_core_plane):
		core_plane_index=i+1
		for j in xrange(num_hosts):
			host_index=j+1
			host_name="h%d_1_%d" %(core_plane_index,host_index)
			h=net.get(host_name)

			tor_name="st%d_1" %(core_plane_index)
			tor=net.get(tor_name)
			tor_ip=tor.IP(intf=tor_name+"-eth%d"%(MAX_PORT-host_index))
			tor_mac=tor.MAC(intf=tor_name+"-eth%d"%(MAX_PORT-host_index))
			h.setARP(tor_ip, tor_mac)
			h.setDefaultRoute("dev eth0 via %s" % tor_ip)


	# 
	# num_hosts
	# num_core_plane
	# num_core_switch_in_plane
	# num_tor_in_plane
	# num_aggregate_switch

	

	#generate all Core commands
	print('===generate all Core commands')
	thrift_port=9090-1
	# sc: 1 to num_core_switch_in_plane ,  num_core_switch_in_plane+num_aggregate_switch+ num_tor_in_plane to 
	for i in xrange(num_core_plane): #every core plane
		current_core_index=i+1;
		for j in xrange(num_core_switch_in_plane): #every core switch
			current_core_switch_index=j+1
			core_name="sc%d_%d" %(current_core_index, current_core_switch_index) 
			my_core_switch=net.get(core_name)
			thrift_port+=1
			print(str(thrift_port))
			f = open(str(thrift_port)+'.txt','w')
			f.write('table_set_default switch_role_table init_switch_role 3\n')	
			# f.write('table_set_default send_frame _drop\n') 
			f.write('table_set_default forward _drop\n') 
			f.write('table_set_default set_CLI_thrift_port_table set_CLI_thrift_port '+str(thrift_port)+'\n');
			for k in xrange(num_core_plane):
				f.write('table_indirect_create_group ecmp_group\n')
			f.write('table_indirect_create_member ecmp_group _drop\n')
			for k in xrange(num_core_plane):# 1 core switch need to conned to (num_core_plane) aggregate switch
				current_core_for_aggregate_index=k+1
				aggregate_name='sa%d_%d' % (current_core_for_aggregate_index,current_core_index )#connect to (all_core, same core)
				print("aggregate_name"+aggregate_name+"\n")

				my_aggregate_switch=net.get(aggregate_name)
				my_aggregate_switch_ip=my_aggregate_switch.IP(intf=aggregate_name+"-eth"+str(current_core_switch_index))

				print(aggregate_name+"-eth"+str(current_core_switch_index))
				switch_inf_map_ip[aggregate_name+"-eth"+str(current_core_switch_index)] = my_aggregate_switch_ip
				my_aggregate_switch_mac=my_aggregate_switch.MAC(intf=aggregate_name+"-eth"+str(current_core_switch_index))
				my_core_switch_ip=my_core_switch.IP(intf=core_name+"-eth"+str(current_core_for_aggregate_index))
				f.write('table_add forward set_dmac '+my_aggregate_switch_ip+" => "+my_aggregate_switch_mac+'\n') 
				f.write('table_indirect_create_member ecmp_group set_nhop '+my_aggregate_switch_ip+" "+str(current_core_for_aggregate_index)+'\n') 
			for k in xrange(num_core_plane):
				f.write('table_indirect_add_member_to_group ecmp_group '+str(k+1)+' '+str(k)+'\n') 
			f.write('table_indirect_set_default ecmp_group 0\n')
			for k in xrange(num_core_plane):
				core_plane_for_host_index=k+1
				for m in xrange(num_hosts):
					host_index=m+1
					current_host_name='h%d_%d_%d' %(core_plane_for_host_index, 1, host_index)
					current_host=net.get(current_host_name)
					current_host_ip=current_host.IP(intf=current_host_name+"-eth0")
					# if core_plane_for_host_index==core_plane_index
					# 	f.write('table_indirect_add_with_group ecmp_group '+current_host_ip+'/32 => 1\n')
					# else
					# 	f.write('table_indirect_add_with_group ecmp_group '+current_host_ip+'/32 => 0\n')
					f.write('table_indirect_add_with_group ecmp_group '+current_host_ip+'/32 => '+str(k)+'\n')
			f.close() 
		thrift_port=thrift_port+num_aggregate_switch+num_tor_in_plane
	

	same_as_ecmp_group=['ecmp_group','expeditus_aggregate_group','expeditus_stage2_aggregate_dst_group']#
	same_as_forward=['forward','expeditus_aggregate_forward','expeditus_stage2_aggregate_dst_forward']#


	#generating all Aggregate commands
	print('===generating all Aggregate commands')
	thrift_port=9089+num_core_switch_in_plane
	for i in xrange(num_core_plane):#each core plane
		core_plane_index=i+1
		for j in xrange(num_aggregate_switch):#each aggregate switch
			aggregate_index=j+1
			thrift_port+=1
			member_number=0
			print(str(thrift_port))
			f = open(str(thrift_port)+'.txt','w')
			f.write('table_set_default Aggregate_dst_3rd_stage_table Aggregate_dst_3rd_stage_action\n');
			f.write('table_set_default Aggregate_clone_egress_to_egress_table Aggregate_clone_egress_to_egress_action\n');
			f.write('table_set_default Aggregate_stage_3_modify_fields_table Aggregate_stage_3_modify_fields_action\n');
			f.write('table_set_default Aggregate_dst_3rd_stage_table Aggregate_dst_3rd_stage_action\n');
			f.write('table_set_default expeditus_stage2_aggregate_src_compute_egress_port expeditus_stage2_aggregate_compute_and_set_later_forward_egress '+str(num_core_switch_in_plane)+'\n')
			f.write('table_set_default expeditus_stage2_aggregate_dst_header_table expeditus_stage2_aggregate_dst_header_action\n')
			f.write('table_set_default switch_role_table init_switch_role 2\n')	
			f.write('table_set_default ingress_upstream_table get_upstream_num '+str(num_core_switch_in_plane)+'\n')
			f.write('table_set_default egress_upstream_table get_upstream_num '+str(num_core_switch_in_plane)+'\n')
			# f.write('table_set_default send_frame _drop\n') 
			for  ecmp_forward_string in same_as_forward:
				f.write('table_set_default '+ecmp_forward_string+' _drop\n') 
			f.write('table_set_default set_CLI_thrift_port_table set_CLI_thrift_port '+str(thrift_port)+'\n');	
			for k in xrange(num_tor_in_plane+1):#num_tor_in_plane + upstream
				for ecmp_group_string in same_as_ecmp_group:
					f.write('table_indirect_create_group '+ecmp_group_string+'\n') 	
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_create_member '+ecmp_group_string+' _drop\n') 	
			for k in xrange(num_core_switch_in_plane): #num_core_switch_in_plane
				core_switch_index=k+1
				# for m in xrange(num_core_switch_in_plane):
				core_switch_name='sc%d_%d' %(aggregate_index,core_switch_index)
				print(core_switch_name+'\n')
				current_core_switch=net.get(core_switch_name)
				current_core_switch_ip=current_core_switch.IP(intf=core_switch_name+"-eth"+str(core_plane_index))
				current_core_switch_mac=current_core_switch.MAC(intf=core_switch_name+"-eth"+str(core_plane_index))
				print(core_switch_name+"-eth"+str(core_switch_index) +" "+current_core_switch_ip)
				for  ecmp_forward_string in same_as_forward:
					f.write('table_add '+ecmp_forward_string+' set_dmac '+current_core_switch_ip+" => "+current_core_switch_mac+'\n') 
				for ecmp_group_string in same_as_ecmp_group:
					f.write('table_indirect_create_member '+ecmp_group_string+' set_nhop '+current_core_switch_ip+' '+str(core_switch_index)+'\n') 	
				member_number+=1
				for ecmp_group_string in same_as_ecmp_group:
					f.write('table_indirect_add_member_to_group '+ecmp_group_string+' '+str(member_number)+' 0\n') 	


			tor_switch_name='st%d_%d'  % (core_plane_index, 1)
			print(tor_switch_name+"\n")
			current_tor_switch=net.get(tor_switch_name)
			# current_tor_switch_ip=current_tor_switch.IP(intf=tor_switch_name+"-eth"+str(aggregate_index))
			current_tor_switch_ip=current_tor_switch.IP(intf=tor_switch_name+"-eth"+str(aggregate_index))
			print(tor_switch_name+"-eth"+str(aggregate_index))
			current_tor_switch_mac=current_tor_switch.MAC(intf=tor_switch_name+"-eth"+str(aggregate_index))
			for  ecmp_forward_string in same_as_forward:
				f.write('table_add '+ecmp_forward_string+' set_dmac '+current_tor_switch_ip+" => "+current_tor_switch_mac+'\n') 
			
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_create_member '+ecmp_group_string+' set_nhop '+current_tor_switch_ip+' '+str(MAX_PORT-1)+'\n') 	
			member_number+=1
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_add_member_to_group '+ecmp_group_string+' '+str(member_number)+' 1\n') 	

			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_set_default '+ecmp_group_string+' 0\n') 	
			

			for k in xrange(num_core_plane):
				core_plane_for_host_index=k+1
				for m in xrange(num_hosts):
					host_index=m+1
					current_host_name='h%d_%d_%d' %(core_plane_for_host_index, 1, host_index)
					current_host=net.get(current_host_name)
					current_host_ip=current_host.IP(intf=current_host_name+"-eth0")
					if core_plane_for_host_index==core_plane_index:
						for ecmp_group_string in same_as_ecmp_group:
							f.write('table_indirect_add_with_group '+ecmp_group_string+' '+current_host_ip+'/32 => 1\n')
					else:
						for ecmp_group_string in same_as_ecmp_group:
							f.write('table_indirect_add_with_group '+ecmp_group_string+' '+current_host_ip+'/32 => 0\n')
			for portNumber in xrange(MAX_PORT):
				f.write('mirroring_add '+str(portNumber+1)+" "+str(portNumber+1)+"\n")
			f.close()

		thrift_port=thrift_port+num_tor_in_plane+num_core_switch_in_plane





	# num_hosts
	# num_core_plane
	# num_core_switch_in_plane
	# num_tor_in_plane
	# num_aggregate_switch



	#generate all ToR commands
	same_as_ecmp_group=['ecmp_group','expeditus_group','expeditus_stage1_tor_target_group']#,'expeditus_tor_remaining_group'
	same_as_forward=['forward','expeditus_first_forward','expeditus_stage1_tor_target_forward']#,'expeditus_tor_remaining_forward'

	print('===generate all ToR commands')
	thrift_port=9089
	for i in xrange(num_core_plane):#each core plane, one ToR
		core_plane_index=i+1
		thrift_port+=num_core_switch_in_plane+num_aggregate_switch+1
		print(thrift_port)
		#one ToR switch
		member_number=0
		f = open(str(thrift_port)+'.txt','w')	

		f.write('table_set_default ToR_src_2nd_stage_table ToR_src_2nd_stage_action'+'\n')
		f.write('table_set_default ToR_modify_cloned_packet modify_cloned_packet'+'\n')
		f.write('table_set_default ingress_upstream_table get_upstream_num '+str(num_aggregate_switch)+'\n')
		f.write('table_set_default egress_upstream_table get_upstream_num '+str(num_aggregate_switch)+'\n')
		# f.write('table_set_default send_frame _drop\n')
		for forward_string in same_as_forward:
			f.write('table_set_default %s _drop\n'%forward_string)

		# f.write('table_set_default expeditus_stage1_tor_target_compute_egress_port compute_egress_spec '+num_aggregate_switch+'\n')
		f.write('table_set_default set_CLI_thrift_port_table set_CLI_thrift_port '+str(thrift_port)+'\n');
		f.write('table_set_default expeditus_stage1_tor_target_remove_header remove_expeditus_header\n')
		f.write('table_set_default expeditus_header_table add_expeditus_tags\n')
		f.write('table_set_default increment_table increment_action\n')
		f.write('table_set_default increment_SYN_table increment_SYN_action\n')
		f.write('table_set_default switch_role_table init_switch_role 1\n')
		f.write('table_set_default expeditus_stage1_tor_target_compute_egress_port compute_and_set_later_forward_egress '+str(num_aggregate_switch)+'\n')
		f.write('table_add increment_table increment_action\n')
		for k in xrange(num_hosts+1):  #host with upstream
			for  ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_create_group '+ecmp_group_string+'\n')
				print(ecmp_group_string+"\n")
		for ecmp_group_string in same_as_ecmp_group:
			f.write('table_indirect_create_member '+ecmp_group_string+' _drop\n')
		
		for k in xrange(num_aggregate_switch): # each aggregate 
			aggregate_index=k+1
			aggregate_name='sa%d_%d' % (core_plane_index,aggregate_index)
			current_aggregate=net.get(aggregate_name)
			current_aggregate_ip=current_aggregate.IP(intf=aggregate_name+"-eth"+str(MAX_PORT-1))
			current_aggregate_mac=current_aggregate.MAC(intf=aggregate_name+"-eth"+str(MAX_PORT-1))
			for forward_string in same_as_forward:
				f.write('table_add %s set_dmac '%forward_string+current_aggregate_ip+' => '+current_aggregate_mac+'\n')
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_create_member %s set_nhop ' % ecmp_group_string +current_aggregate_ip+' '+str(aggregate_index)+'\n')
			member_number+=1
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_add_member_to_group %s '%ecmp_group_string+str(member_number)+' 0\n')#upstream is 0
		for k in xrange(num_hosts): # each num_hosts in same core plane
			host_index=k+1
			host_name='h%d_%d_%d' %(core_plane_index, 1, host_index)
			current_host=net.get(host_name)
			current_host_ip=current_host.IP(intf=host_name+"-eth0")
			current_host_mac=current_host.MAC(intf=host_name+"-eth0")
			for forward_string in same_as_forward:
				f.write('table_add %s set_dmac '%forward_string+current_host_ip+' => '+current_host_mac+'\n')
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_create_member %s set_nhop '%ecmp_group_string+current_host_ip+' '+str(MAX_PORT-host_index)+'\n')
			member_number+=1
			for ecmp_group_string in same_as_ecmp_group:
				f.write('table_indirect_add_member_to_group %s '%ecmp_group_string+str(member_number)+' '+str(host_index)+'\n')#upstream is 0
		for ecmp_group_string in same_as_ecmp_group:
			f.write('table_indirect_set_default %s 0\n'%ecmp_group_string)

		for k in xrange(num_core_plane):
			core_for_host_index=k+1

			for m in xrange(num_hosts): # each num_hosts in same core plane
				host_index=m+1
				host_name='h%d_%d_%d' %(core_for_host_index, 1, host_index)
				current_host=net.get(host_name)
				current_host_ip=current_host.IP(intf=host_name+"-eth0")
				current_host_mac=current_host.MAC(intf=host_name+"-eth0")
				if(core_for_host_index==core_plane_index): # same core plane
					print('same: '+host_name)
					for ecmp_group_string in same_as_ecmp_group:
						f.write('table_indirect_add_with_group %s '%ecmp_group_string+current_host_ip+'/32 => '+str(host_index)+'\n')
				else: #not same core plane -> to upstream
					print('diff: '+host_name)
					for ecmp_group_string in same_as_ecmp_group:
						f.write('table_indirect_add_with_group %s '%ecmp_group_string+current_host_ip+'/32 => 0\n')
		for portNumber in xrange(MAX_PORT):
			f.write('mirroring_add '+str(portNumber+1)+" "+str(portNumber+1)+"\n")
		f.close()
	print "Ready !"

	CLI( net )
	net.stop()

if __name__ == '__main__':
	setLogLevel( 'info' )
	main()
