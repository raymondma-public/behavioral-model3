#!/usr/bin/env python2

# Copyright 2013-present Barefoot Networks, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.log import setLogLevel, info
from mininet.cli import CLI

from p4_mininet import P4Switch, P4Host

import argparse
from time import sleep

parser = argparse.ArgumentParser(description='Mininet demo')
parser.add_argument('--behavioral-exe', help='Path to behavioral executable',
                    type=str, action="store", required=True)
parser.add_argument('--thrift-port', help='Thrift server port for table updates',
                    type=int, action="store", default=9090)
parser.add_argument('--num-hosts', help='Number of hosts to connect to switch',
                    type=int, action="store", default=3)
parser.add_argument('--mode', choices=['l2', 'l3'], type=str, default='l3')
parser.add_argument('--json', help='Path to JSON config file',
                    type=str, action="store", required=True)
parser.add_argument('--pcap-dump', help='Dump packets on interfaces to pcap files',
                    type=str, action="store", required=False, default=False)

parser.add_argument('--num-core-plane', help='number of core plane',
                    type=int, action="store", required=False, default=2)
parser.add_argument('--num-core-switch-in-plane', help='number of core switch in 1 plane',
                    type=int, action="store", required=False, default=2)
parser.add_argument('--num-tor-in-plane', help='number of ToR switch in 1 plane',
                    type=int, action="store", required=False, default=1)

args = parser.parse_args()

MAX_PORT=255

class ClosTopo(Topo):
    "Single switch connected to n (< 256) hosts."
    def __init__(self, sw_path, json_path, thrift_port, pcap_dump,n_hosts, n_core_plane,n_core_switch_in_plane,n_tor_in_plane, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        totalNumSwitch=0
        n=n_hosts #num host
        num_switch_in_core=n_core_plane
        num_aggregate_switch=n_core_plane
        print(str(thrift_port)+"\n");



        current_core_plane_number=0
        current_core_switch_number=0
        current_aggregate_switch_number=0
        

        list_of_aggregate_list=[]
        list_of_core_list=[]
        list_of_tor_list=[]


        h_count=0;
        for i in xrange(n_core_plane): #for each of the core plane
            current_core_plane_number=i+1

            list_of_core=[]
            list_of_core_list.append(list_of_core)

            list_of_aggregate=[]
            list_of_aggregate_list.append(list_of_aggregate)

            list_of_tor=[]
            list_of_tor_list.append(list_of_tor)
            for j in xrange(n_core_switch_in_plane): #some core switch
                current_core_switch_number=j+1

                print(str(thrift_port+totalNumSwitch))
                core_s_name="sc"+str(current_core_plane_number)+"_"+str(current_core_switch_number)
                core_s=        switch = self.addSwitch(core_s_name,
                                sw_path = sw_path,
                                json_path = json_path,
                                thrift_port = (thrift_port+totalNumSwitch),
                                pcap_dump = pcap_dump)
                list_of_core.append(core_s)
                print(core_s)

                totalNumSwitch+=1

            for j in xrange(num_aggregate_switch): #some aggregate switch
                current_aggregate_switch_number=j+1
                print(str(thrift_port+totalNumSwitch))
                aggregate_s_name="sa"+str(current_core_plane_number)+"_"+str(current_aggregate_switch_number)
                aggregate_s=     switch = self.addSwitch(aggregate_s_name,
                                sw_path = sw_path,
                                json_path = json_path,
                                thrift_port = (thrift_port+totalNumSwitch),
                                pcap_dump = pcap_dump)
                list_of_aggregate.append(aggregate_s)
                print(aggregate_s)
                totalNumSwitch+=1

            # current_tor_index=0
            for j in xrange(n_tor_in_plane): #some tor switch
                current_tor_index=j+1

                print(str(thrift_port+totalNumSwitch))
                tor_s_name="st"+str(current_core_plane_number)+"_"+str(current_tor_index)
                tor_s=switch = self.addSwitch(tor_s_name,
                                sw_path = sw_path,
                                json_path = json_path,
                                thrift_port = (thrift_port+totalNumSwitch),
                                pcap_dump = pcap_dump)
                for k in xrange(n):
                    host_index=k+1
                    host_name="h"+str(current_core_plane_number)+"_"+str(current_tor_index)+"_"+str(host_index)
                    h_count+=1
                    print("host: "+str(h_count))
                    host = self.addHost(host_name,
                                    ip = "10.0.%d.4/24" % h_count,
                                    mac = '00:04:00:00:%02x:00' %h_count
                                    )
                    self.addLink(tor_s,host,MAX_PORT-host_index,0)

                list_of_tor.append(tor_s)
                print(tor_s)
                totalNumSwitch+=1

        print("core")
        for list in list_of_core_list:
            print ','.join(list)

        print("aggregate")
        for list in list_of_aggregate_list:
            print ','.join(list)

        print("tor")
        for list in list_of_tor_list:
            print ','.join(list)

        tor_index=0

        # #left Tor

        # print("st1")
        # totalNumSwitch+=1

        # #right Tor
        # print("st2")
        # totalNumSwitch+=1


        print("\n\nconnections")
        core_plane_index=0
        
        for core_list in list_of_core_list:
            core_plane_index+=1

            core_index=0;
            for core in core_list:
                core_index+=1

                aggregate_list_index=0
                for aggregate_list in list_of_aggregate_list:#[core_plane_index-1]
                    aggregate_list_index+=1
                    print ("- "+core+" "+aggregate_list[core_plane_index-1]+ " "+ str(aggregate_list_index) +" "+str(core_index))
                    self.addLink(core, aggregate_list[core_plane_index-1],aggregate_list_index,core_index)


            aggregate_index=0
            tor_index=0
            for tor in list_of_tor_list[core_plane_index-1]:
                tor_index+=1
                aggregate_index=0
                for aggregate in list_of_aggregate_list[core_plane_index-1]:
                    aggregate_index+=1
                    print (tor+" "+aggregate+" "+str(aggregate_index)+" "+str(MAX_PORT-tor_index))
                    self.addLink(tor, aggregate,aggregate_index,MAX_PORT-tor_index)




        print(str(thrift_port+totalNumSwitch)+"\n")
        print("totalNumSwitch"+str(totalNumSwitch))



        switch = self.addSwitch('s1',
                                sw_path = sw_path,
                                json_path = json_path,
                                thrift_port = 9089,
                                pcap_dump = pcap_dump)


        for h in xrange(n):
            host = self.addHost('h%d' % (h + 1),
                                ip = "10.0.%d.10/24" % h,
                                mac = '00:04:00:00:00:%02x' %h)
            self.addLink(host, switch)



def main():
    num_hosts= args.num_hosts
    num_core_plane = args.num_core_plane
    num_core_switch_in_plane= args.num_core_switch_in_plane
    num_tor_in_plane= args.num_tor_in_plane
    num_aggregate_switch=num_core_plane

    mode = args.mode

    topo = ClosTopo(args.behavioral_exe,
                            args.json,
                            args.thrift_port,
                            args.pcap_dump,
                            num_hosts,
                            num_core_plane,
                            num_core_switch_in_plane,
                            num_tor_in_plane
                            )


    net = Mininet(topo = topo,
                  host = P4Host,
                  switch = P4Switch,
                  controller = None)
    net.start()


    sw_mac = ["00:aa:bb:00:00:%02x" % n for n in xrange(num_hosts)]

    sw_addr = ["10.0.%d.1" % n for n in xrange(num_hosts)]

    for i in xrange(num_core_plane):
        plane_index=i+1

        for j in xrange(num_core_switch_in_plane):
            aggregate_index=j+1

            index_string=str(plane_index)+'_'+str(aggregate_index)

            
            core_name='sc'+index_string
            currentSwitch=net.get(core_name)
            # num_switch+=1
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
            # num_switch+=1
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')

            # num_switch+=1
            aggregate_name='sa'+index_string
            currentSwitch=net.get(aggregate_name)
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
            # num_switch+=1
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')


        for j in xrange(num_tor_in_plane):
            tor_index=j+1

            # num_switch+=1
            tor_name='st'+str(plane_index)+'_'+str(tor_index)
            currentSwitch=net.get(tor_name)
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth1')
            # num_switch+=1
            # currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='eth2')

    for n in xrange(num_hosts):
        h = net.get('h%d' % (n + 1))
        if mode == "l2":
            h.setDefaultRoute("dev eth0")
        else:
            h.setARP(sw_addr[n], sw_mac[n])
            h.setDefaultRoute("dev eth0 via %s" % sw_addr[n])

    for n in xrange(num_hosts):
        h = net.get('h%d' % (n + 1))
        h.describe()



    for i in xrange(num_core_plane):
        plane_index=i+1
        for j in xrange(num_tor_in_plane):
            tor_index=j+1
            for k in xrange(num_hosts):
                host_index=k+1
                host_name="h"+str(plane_index)+"_"+str(tor_index)+"_"+str(host_index)
                h=net.get(host_name)
                if mode == "l2":
                    h.setDefaultRoute("dev eth0")
                else:
                    h.setARP(sw_addr[n], sw_mac[n])
                    h.setDefaultRoute("dev eth0 via %s" % sw_addr[n])
                h.describe()

    total_num_core_switch_inf=0
    total_num_aggregate_inf=0
    total_num_tor_inf=0
    total_num_switch=0
    for i in xrange(num_core_plane):
        core_plane_index=i+1


        for j in xrange(num_core_switch_in_plane):
            core_switch_index=j+1

            core_name="sc%d_%d" % (core_plane_index,core_switch_index)
            mySwitch=net.get(core_name)
            for k in xrange (num_core_plane):
                total_num_core_switch_inf+=1
                total_num_switch+=1
                mySwitch.setMAC('00:0a:00:00:00:%02x' % total_num_core_switch_inf,intf=core_name+str("-eth%d" % (k+1)));
                inf_name=core_name+str("-eth%d" % (k+1))
                print("inf_name:"+inf_name+"\n\n\n")
                mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=inf_name)
                print(mySwitch.IP(core_name+str("-eth%d" % (k+1))))

        for j in xrange(num_aggregate_switch):
            aggregate_index=j+1

            aggregate_name="sa%d_%d" % (core_plane_index,aggregate_index)
            mySwitch=net.get(aggregate_name)
            for k in xrange(num_core_plane):
                total_num_aggregate_inf+=1
                total_num_switch+=1
                mySwitch.setMAC('00:0b:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (k+1)));
                mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (k+1)))
                print(mySwitch.IP(aggregate_name+str("-eth%d" % (k+1))))

            for k in xrange(num_tor_in_plane):
                total_num_aggregate_inf+=1
                total_num_switch+=1
                mySwitch.setMAC('00:0b:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)));
                mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)))
                print(mySwitch.IP(aggregate_name+str("-eth%d" % (MAX_PORT-k-1))))
        for j in xrange(num_tor_in_plane):
            aggregate_index=j+1

            aggregate_name="st%d_%d" % (core_plane_index,aggregate_index)
            mySwitch=net.get(aggregate_name)
            for k in xrange(num_core_plane):
                total_num_aggregate_inf+=1
                total_num_switch+=1
                mySwitch.setMAC('00:0c:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (k+1)));
                mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (k+1)))
                print(mySwitch.IP(aggregate_name+str("-eth%d" % (k+1))))
            for k in xrange(num_hosts):
                total_num_aggregate_inf+=1
                total_num_switch+=1
                mySwitch.setMAC('00:0c:00:00:00:%02x' %total_num_aggregate_inf,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)));
                mySwitch.setIP('10.1.0.%d/24' % total_num_switch,intf=aggregate_name+str("-eth%d" % (MAX_PORT-k-1)))
                print(mySwitch.IP(aggregate_name+str("-eth%d" % (MAX_PORT-k-1))))


        # for j in xrange(num_tor_in_plane):
        #     tor_index=j+1
        #     tor_name="st%d_%d" % (core_plane_index,tor_index)
        #     mySwitch=net.get(tor_name)
        #     for k in xrange(num_core_plane):
        #         total_num_tor_inf+=1
        #         mySwitch.setMAC('03:00:00:00:00:%02x' %total_num_tor_inf,tor_name+str("-eth%d" % (k+1)));
        #     for k in xrange(num_hosts):
        #         total_num_tor_inf+=1
        #         mySwitch.setMAC('03:00:00:00:00:%02x' %total_num_tor_inf,tor_name+str("-eth%d" % (MAX_PORT-k-1)));


    currentSwitch=net.get("s1");
    num_switch=0
    num_switch+=1
    currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='s1-eth1')
    num_switch+=1
    currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='s1-eth2')        
    

    print "Ready !"

    CLI( net )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    main()
