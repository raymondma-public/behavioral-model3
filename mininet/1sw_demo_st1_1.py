#!/usr/bin/env python2

# Copyright 2013-present Barefoot Networks, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from mininet.net import Mininet
from mininet.topo import Topo
from mininet.log import setLogLevel, info
from mininet.cli import CLI

from p4_mininet import P4Switch, P4Host

import argparse
from time import sleep

parser = argparse.ArgumentParser(description='Mininet demo')
parser.add_argument('--behavioral-exe', help='Path to behavioral executable',
                    type=str, action="store", required=True)
parser.add_argument('--thrift-port', help='Thrift server port for table updates',
                    type=int, action="store", default=9090)
parser.add_argument('--num-hosts', help='Number of hosts to connect to switch',
                    type=int, action="store", default=2)
parser.add_argument('--mode', choices=['l2', 'l3'], type=str, default='l3')
parser.add_argument('--json', help='Path to JSON config file',
                    type=str, action="store", required=True)
parser.add_argument('--pcap-dump', help='Dump packets on interfaces to pcap files',
                    type=str, action="store", required=False, default=False)

args = parser.parse_args()

max_port=255

class SingleSwitchTopo(Topo):
    "Single switch connected to n (< 256) hosts."
    def __init__(self, sw_path, json_path, thrift_port, pcap_dump, n, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        switch = self.addSwitch('st1_1',
                                sw_path = sw_path,
                                json_path = json_path,
                                thrift_port = thrift_port,
                                pcap_dump = pcap_dump)
 

        for h in xrange(n):
            host = self.addHost('h1_1_%d' % (h + 1),
                                ip = "10.0.%d.4/24" % (h+1),
                                mac = '00:04:00:00:%02x:00' %h)
            self.addLink(host, switch,0,(max_port-h-1))

def main():
    num_hosts = args.num_hosts
    mode = args.mode

    topo = SingleSwitchTopo(args.behavioral_exe,
                            args.json,
                            args.thrift_port,
                            args.pcap_dump,
                            num_hosts)
    net = Mininet(topo = topo,
                  host = P4Host,
                  switch = P4Switch,
                  controller = None)
    net.start()


    sw_mac = ["00:0c:00:00:00:%02x" % n for n in xrange(num_hosts+1)]

    sw_addr = ["10.0.%d.2" % n for n in xrange(num_hosts+1)]

    for n in xrange(num_hosts):
        h = net.get('h1_1_%d' % (n + 1))
        if mode == "l2":
            h.setDefaultRoute("dev eth0")
        else:
            h.setARP(sw_addr[n], sw_mac[n])
            h.setDefaultRoute("dev eth0 via %s" % sw_addr[n+1])

    for n in xrange(num_hosts):
        h = net.get('h1_1_%d' % (n + 1))
        h.describe()

    sleep(1)


    currentSwitch=net.get("st1_1");
    num_switch=0

    num_switch+=1
    currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='st1_1-eth'+str(max_port-num_switch))
    currentSwitch.setMAC(mac='00:0c:00:00:00:%02x' % 9,intf='st1_1-eth'+str(max_port-num_switch));
    num_switch+=1
    currentSwitch.setIP(ip="10.0.%d.2/24" % num_switch, intf='st1_1-eth'+str(max_port-num_switch))
    currentSwitch.setMAC(mac='00:0c:00:00:00:%02x' % 10,intf='st1_1-eth'+str(max_port-num_switch));


    print "Ready !"

    CLI( net )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    main()
