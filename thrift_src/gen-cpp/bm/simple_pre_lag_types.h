/**
 * Autogenerated by Thrift Compiler (0.9.2)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef simple_pre_lag_TYPES_H
#define simple_pre_lag_TYPES_H

#include <iosfwd>

#include <thrift/Thrift.h>
#include <thrift/TApplicationException.h>
#include <thrift/protocol/TProtocol.h>
#include <thrift/transport/TTransport.h>

#include <thrift/cxxfunctional.h>


namespace bm_runtime { namespace simple_pre_lag {

struct McOperationErrorCode {
  enum type {
    TABLE_FULL = 1,
    INVALID_HANDLE = 2,
    INVALID_MGID = 3,
    INVALID_L1_HANDLE = 4,
    INVALID_L2_HANLDE = 5,
    ERROR = 6
  };
};

extern const std::map<int, const char*> _McOperationErrorCode_VALUES_TO_NAMES;

typedef int32_t BmMcMgrp;

typedef int32_t BmMcRid;

typedef int32_t BmMcMgrpHandle;

typedef int32_t BmMcL1Handle;

typedef int16_t BmMcLagIndex;

typedef std::string BmMcPortMap;

typedef std::string BmMcLagMap;

class InvalidMcOperation;

typedef struct _InvalidMcOperation__isset {
  _InvalidMcOperation__isset() : code(false) {}
  bool code :1;
} _InvalidMcOperation__isset;

class InvalidMcOperation : public ::apache::thrift::TException {
 public:

  static const char* ascii_fingerprint; // = "8BBB3D0C3B370CB38F2D1340BB79F0AA";
  static const uint8_t binary_fingerprint[16]; // = {0x8B,0xBB,0x3D,0x0C,0x3B,0x37,0x0C,0xB3,0x8F,0x2D,0x13,0x40,0xBB,0x79,0xF0,0xAA};

  InvalidMcOperation(const InvalidMcOperation&);
  InvalidMcOperation& operator=(const InvalidMcOperation&);
  InvalidMcOperation() : code((McOperationErrorCode::type)0) {
  }

  virtual ~InvalidMcOperation() throw();
  McOperationErrorCode::type code;

  _InvalidMcOperation__isset __isset;

  void __set_code(const McOperationErrorCode::type val);

  bool operator == (const InvalidMcOperation & rhs) const
  {
    if (!(code == rhs.code))
      return false;
    return true;
  }
  bool operator != (const InvalidMcOperation &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const InvalidMcOperation & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

  friend std::ostream& operator<<(std::ostream& out, const InvalidMcOperation& obj);
};

void swap(InvalidMcOperation &a, InvalidMcOperation &b);

}} // namespace

#endif
